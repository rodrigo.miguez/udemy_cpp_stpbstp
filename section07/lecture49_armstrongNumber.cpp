#include <iostream>
#include <cmath>

int nDigits(int a) {

  if (a == 0) {
    std::cout << "ERROR: This is a invalid number!" << std::endl;
    return EXIT_FAILURE;  
  }
  
  int numberDigits {0};
  while (a != 0) {
    a /=  10;
    ++numberDigits;
  }

  return numberDigits;
}

bool armstrongNumber(int n) {

  int numberOfDigits = nDigits(n);
  
  int sum {0}, temp {n}, remainder;
  while (n > 0) {
    remainder = n % 10;
    n = n / 10;
    sum += pow(remainder, numberOfDigits);
  }

  if (temp == sum) {
    return true;
  } else {
    return false;
  }

}

int main (int argc, char const *argv[]) {

  /*
  int n;
  std::cout << "Enter any number: ";
  std::cin >> n;

  bool isArmstrongNumber = armstrongNumber(n);

  if (isArmstrongNumber) {
    std::cout << "The number is an Armstrong number!" << std::endl;
  } else {
    std::cout << "The number is not an Armstrong number! << std::endl;
  }
  */

  for (int i = 1; i <= 10000; ++i) {
    if (armstrongNumber(i)) {
      std::cout << i << std::endl;
    }
  }

  return 0;
}
