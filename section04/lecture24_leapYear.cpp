#include <iostream>
#include <typeinfo>

int main (int argc, char const *argv[]) {

  int year {};
  std::cout << "Enter year: ";
  std::cin >> year;

/*
  if ( year % 4 != 0 ) {
    std::cout << year << " is not a leap year." << std::endl;
  } else if ( year % 100 != 0 )  {
    std::cout << year << " is a leap year." << std::endl;
  } else if ( year % 400 == 0 ) {
    std::cout << year << " is a leap year." << std::endl;
  } else {
    std::cout << year << " is not a leap year." << std::endl;
  }
*/

  if ((year % 4 == 0 && year % 100 != 0) || (year % 400 == 0)) {
    std::cout << year << " is a leap year." << std::endl;
  } else {
    std::cout << year << " is not a leap year." << std::endl;
  }

  return 0;
}
