#include <iostream>
#include <fstream>
#include <cstdlib>

using namespace std;

int main() {

  ifstream fin;
  fin.open("Files/codingExercise37.txt");
  if (!fin) {
    cout << "Error while opening file" << endl;
    exit(1);
  }

  int n, buffer, sum = 0;
  fin >> n;

  for (int i = 0; i < n; i++) {
    fin >> buffer;
    sum = sum + buffer;
  }

  double avg = (double)sum/n;
  cout << avg << endl;

  fin.close();

  return 0;
}
