#include <iostream>
#include <sstream>
#include <string>

using namespace std;

int main() {

  // In case your string is separated by commma:
  string str = "10#20#30#40#50#20";

  istringstream iss (str); // iss is an object for the class istringstream

  int k, sum = 0, count = 0;
  while(iss >> k) {
    sum = sum + k;
    count++;
    if (iss.peek() == '#') {
      iss.ignore();
    }
  }

  cout << "sum = " << sum << ", count = " << count << ", average = " << (double)sum/count << endl;

  return 0;
}
