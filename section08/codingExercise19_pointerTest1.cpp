#include <iostream>

using namespace std;
// Please do not change anything in the following function.
void test(int *ptr){
    std::cout << "Testing: " << *ptr << std::endl;
    *ptr = *ptr + 10;

}

int callMe(int value){
// ---- NO CHANGE ABOVE THIS LINE, Write anything after this line
// Instruction 1:  Declare an integer pointer and assign the address of parameter value into that pointer
  int *p = &value;


// Instruction 2: Call function test and pass the pointer as parameter to function test.
  test(p);

// Do not write anything after this line:
  std::cout << "Testing: " << value << endl;

  return value;
}
