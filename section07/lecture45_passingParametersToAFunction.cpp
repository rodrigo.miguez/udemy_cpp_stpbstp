#include <iostream>

// function calling by value: if we change the content of the formal arguments inside of the
// totalPay function (example: hoursWorked = 10;), then if would not change hours_worked inside
// of the main fuction.
double totalPay(int hoursWorked, double ratePerHour) { // (int hoursWorked, double ratePerHour)
                                                            // are called formal arguments
                                                            // (receivers)
  // hoursWorked = 10; // This will not reflect in a change in the hours_worked inside of the main
  // function. That is: any chamge in the formal arguments will not reflect in a change in the
  // actual arguments ---> function calling by value!
  double total = hoursWorked * ratePerHour;
  if (hoursWorked > 40) {
    total = total + (hoursWorked - 40) * 2;
  }

  return total;
  // If the function is returning a value its datatype should be specified before the name of the
  // function (return type) in the function header
}

int main (int argc, char const *argv[]) {

  int hours_worked;
  double rate;

  hours_worked = 41;
  rate = 10.0;

  double total = totalPay(hours_worked, rate); // (hours_worked, rate) are called actual arguments
  std::cout << "Total pay = " << total << std::endl;

  hours_worked = 47;
  rate = 10.5;
  total = totalPay(hours_worked, rate);
  std::cout << "Total pay = " << total << std::endl;

  return 0;
}
