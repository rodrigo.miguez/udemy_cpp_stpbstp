#include <iostream>
#include <typeinfo>

int main(int argc, char* argv[])
{

  int a {10}, b {20};

  // The relational operators are: >  (greater than)
  //                               <  (less than)
  //                               >= (greater equal to)
  //                               <= (less equal to)
  //                               == (equal to)
  //                               != (not equal to)

  // The result of a relational opeator gives a boolean (true or false, 1 or 0)
  bool boolTrue = true, boolFalse = false;
  std::cout << "boolTrue = "  << boolTrue
            << ", boolFalse = " << boolFalse << std::endl;

  bool p = a > b;
  std::cout << "a = " << a << ", b = " << b << std::endl;
  std::cout << "(bool p = a > b;) p = " << p << std::endl;

  p = a < b;
  std::cout << "(bool p = a < b;) p = " << p << std::endl;
  std::cout << "Type of p: " << typeid(p).name() << std::endl;
  std::cout << "Type of (10!=20): " << typeid(10!=20).name() << std::endl;

  return 0;
}
