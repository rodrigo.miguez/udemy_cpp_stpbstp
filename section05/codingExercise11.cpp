#include <iostream>

using namespace std;

int main () {

    // --- DO NOT CHANGE ABOVE THIS LINE. ---
    // You will need to test for the valid n. Read the instructions carefully and proceed.
    // You should write your code after this line.
    int n {0};

    cout << "Enter a number: ";
    cin >> n;

    if (n <= 0) {
        std::cout << "\nINVALID LINE NUMBERS." << std::endl;
        return EXIT_FAILURE;
    }

    std::cout << std::endl;
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j <= i; ++j) {
          cout << "*";
        }
        for (int k = 0; k < i; ++k) {
          cout << "*";
        }
        cout << endl;
    }


}
