#include <iostream>
#include <vector>
#include <cstdlib>

using namespace std;

void display(vector <int> &v) {
  cout << "Size: " << v.size() << ", Capacity: " << v.capacity() << endl;
  cout << "Content of the vector: " << endl;
  for (int p:v) {
    cout << p << ", ";
  }
  cout << "\n" << endl;
}


int main () {

	  vector<int> v {5, 10, 15, 20, 50, 200, 30};
    cout << "Original vector\n";
    display(v);

    // Instruction 1: check the size of the vector, if the size is less than 3 elements then use return to return back
    // 0 to the tester, You do not need to perform anything if the size is < 3, just return 0.
    if (v.size() < 3) {
      cout << "The vector has less than 3 elements!" << endl;
      exit(0);
    }

    // You will proceed with instruction 2  and onwards only if the size of the vector v in >= 3
    // -----------------------------------------------------------------------------------------
    // Instruction 2: add 100 with the front element of the vector, that is, with the existing first element you should add 100. If the current first element is 5, after this operation it should be 105
    v.front() = v[0]+100;
    display(v);

    // Instruction 3: erase the last element of the vector
    v.pop_back();
    display(v);

    // Instruction 4: insert 35 at the rear of the vector, that is after the insertion 35 should be the last integer in the vector
    v.push_back(35);

    // Instruction 5: Insert 110 as the 3rd element of the vector, so after the insert 110 should be the 3rd element. 
    v.insert(v.begin()+2,110);
    display(v);

    // Instruction 6: erase the second element of the vector, now 110 will become the second element
    v.erase(v.begin()+1);
    display(v);

    // Instruction 7: Find the sum of all the elements of the vector v now and then return the sum using the return statement. 
    int sum = 0;
    for (int p:v) {
        sum += p;
    }

    cout << sum << endl;

}
