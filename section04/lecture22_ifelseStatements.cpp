#include <iostream>
#include <typeinfo>

int main(int argc, char const *argv[]) {

  unsigned int project {};
  std::cout << "Projects available:\n" << "1. Teen age verification\n"
                                       << "2. Maximum value\n";
  std::cout << "Enter the project you want to run: ";
  std::cin >> project;

  // Sanitize check
  if ( project != 1 && project != 2 ) {
    std::cout << "This project number is not available!" << std::endl;
    return EXIT_FAILURE;
  }

  if ( project == 1 ) {

    int age {};
    std::cout << "Enter an age: ";
    std::cin >> age;

    if ( age >= 13 && age <= 19 ) {
      std::cout << "Yes, the given age is a teen age!" << std::endl;
    } else {
      std::cout << "No, the given age is not teen age!" << std::endl;
    }

  } else {

    int first {};
    int second {};
    std::cout << "Enter first number: ";
    std::cin >> first;

    std::cout << "Enter second number: ";
    std::cin >> second;

    if ( first > second ) {
      std::cout << "Maximum is: " << first << std::endl;
    } else if ( first == second ) {
      std::cout << "Both the numbers are exactly equal" << std::endl;
    } else {
      std::cout << "Maximum is: " << second << std::endl;
    }

  }

  return 0;
}
