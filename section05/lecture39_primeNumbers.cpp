#include <iostream>
#include <cmath>
// program to find the prime numbers

int main (int argc, char const *argv[]) {

  int n {0}, count {2};
  std::cout << "Enter the number of terms: ";
  std::cin >> n;

  std::cout << "Factors are: 1 ";
  for (int i = 2; i <= sqrt(n); ++i) {
    if (n % i == 0) {
      std::cout << i << " ";
      ++count;
      if (i != n/i) {
        std::cout << n/i << " ";
        ++count;
      }
    }
  }
  std::cout << n << " " << std::endl;
  std::cout << "Total number of factors found is " << count << std::endl;

  if (count == 2) {
    std::cout << "The given number is a prime number" << std::endl;
  } else {
    std::cout << "The given number is not a prime number" << std::endl;
  }

  return 0;
}
