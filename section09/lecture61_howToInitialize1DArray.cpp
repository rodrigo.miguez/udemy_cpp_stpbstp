#include <iostream>

int main (int argc, char const *argv[]) {

  std::cout << "The whole array is being initizalied:"  << std::endl;
  int x[5] = {10, 20, 30, 40, 50};
  for (int i = 0; i < sizeof(x)/sizeof(int); ++i) {
    std::cout << x[i] << " ";
  }
  std::cout << "\n" << std::endl;

  // To initialize only the first and second elements: int y[5] = {10, 20};
  // This will make the rest of the elements to be initialized to 0 by the compiler.
  std::cout << "The first two array elements are being initizalied:" << std::endl;
  int y[5] = {10, 20};
  for (int i = 0; i < sizeof(y)/sizeof(int); ++i) {
    std::cout << y[i] << " ";
  }
  std::cout << "\n" << std::endl;


  // In case the array is not initialized, then all the elements will have garbage values
  std::cout << "The  array elements are not being initizalied:" << std::endl;
  int z[5];
  for (int i = 0; i < sizeof(z)/sizeof(int); ++i) {
    std::cout << z[i] << " ";
  }
  std::cout << "\n" << std::endl;

  // In case the array is not initialized, then all the elements will have garbage values
  std::cout << "We can also initialize an array without giving its size:" << std::endl;
  int w[] = {5, 15, 25};
  for (int i = 0; i < sizeof(w)/sizeof(int); ++i) {
    std::cout << w[i] << " ";
  }
  std::cout << std::endl;
  
  return 0;
}
