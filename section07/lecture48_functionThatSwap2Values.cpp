#include <iostream>

void swap(int &a, int &b) {
  int temp = a;
  a = b;
  b = temp;
}

int main (int argc, char const *argv[]) {

  int first {10}, second {20};
  std::cout << "Before calling the function: first = " << first << ", second = " << second << std::endl;
  swap(first, second);
  std::cout << "After calling the function:  first = " << first << ", second = " << second << std::endl;

  return 0;
}
