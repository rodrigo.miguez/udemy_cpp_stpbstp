#include <iostream>

int main(int argc, char* argv[])
{

  // The logical operators are: && (and)
  //                            || (or)
  //                             ! (not)

  std::cout << "&& Operator:" << std::endl;
  int age {10};
  bool r = age >= 13 && age <= 19;
  std::cout << "age = " << age << std::endl;
  std::cout << "(r = age >= 13 && age <= 19;) r = " << r << "\n" << std::endl;

  std::cout << "|| Operator:" << std::endl;
  age = 65;
  r = age <= 10 || age >= 60;
  std::cout << "age = " << age << std::endl;
  std::cout << "(r = age <= 10 || age >= 60;) r = " << r << "\n" << std::endl;

  std::cout << "! Operator:" << std::endl; // Operates in a single operand
  age = 10;
  r = !(age >= 13 && age <= 19);
  std::cout << "age = " << age << std::endl;
  std::cout << "[!(r = age >= 13 && age <= 19);] r = " << r << "\n" << std::endl;

  return 0;
}
