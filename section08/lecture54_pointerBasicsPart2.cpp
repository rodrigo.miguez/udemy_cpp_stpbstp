#include <iostream>

int main(int argx, char const *argv[]) {

  int temp = 300;
  std::cout << "int temp = 300;" << std::endl;
  std::cout << "Address of the variable temp: &temp = " << &temp << std::endl;
  std::cout << "Address of the variable temp: (unsigned long long)&temp = "
            << (unsigned long long)&temp << std::endl;

  int* p;
  p = &temp;
  std::cout << "int* p;\n" << "p = &temp;" << std::endl;
  std::cout << "Pointer: p = " << p << std::endl;
  std::cout << "Dereference operator: *p = " << *p << std::endl;

  // It is also possible to perform operations using the Dereference operator, for example:
  *p = *p + 1; // The result should be 300 + 1 = 301;
  std::cout << "*p = *p + 1 = " << *p << std::endl;

  // Now check what is happening with the initial variable temp;
  std::cout << "temp = " << temp << std::endl;
  // We see that it changes the content stored in the variable temp;

  std::cout << "\n";
  int x {10};
  std::cout << "x = " << x << ", &x = " << &x << std::endl;
  int &y = x;
  std::cout << "int &y = x;" << std::endl;
  std::cout << "y = " << y << ", &y = " << &y << ", *&y = " << *&y << std::endl;

  // Now we are going to check the size of the pointer in our machine, the size of it depends on
  // the system. Usually: 32bits systems gives a pointer of 4 bytes and 64bits systems pointers of
  // 8 bytes.
  std::cout << "\n";
  std::cout << "Checking the size of our pointers in the current system:" << std::endl;
  std::cout << "Size of pointer: sizeof(p) = " << sizeof(p) << std::endl;

  char* k;
  std::cout << "char* k;" << std::endl;
  std::cout << "Size of pointer: sizeof(k) = " << sizeof(k) << std::endl;
  std::cout << "Size of pointer: sizeof(*k) = " << sizeof(*k) << std::endl;
  long double *time;
  std::cout << "long double *time;" << std::endl;
  std::cout << "Size of pointer: sizeof(time) = " << sizeof(time) << std::endl;
  long double distance;
  std::cout << "long double distance; // NOT A POINTER" << std::endl;
  std::cout << "Size of variable: sizeof(distance) = " << sizeof(distance) << std::endl;
  std::cout << "/* The difference in size between the variable 'distance' and the pointer 'time' \n"
            << "** shows that the size of a pointer depends upen the system and not the datatype \n"
            << "*/ of the pointer." << std::endl;

  return 0;
}
