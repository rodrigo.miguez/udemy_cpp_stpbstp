#include <iostream>
#include <string>

using namespace std;

int main() {

  string str ("Hello John! Welcome to my C++ course. Good luck.");
  string target("+. !,");

  string::size_type pos = str.find_first_of(target);
/*
  while (pos != string::npos) {
   cout << "Found '" << str[pos] << "' at: " << pos << endl;
   pos = str.find_first_of(target, pos + 1);
  }
*/

  // To change all occurences to $:
  while (pos != string::npos) {
   str[pos] = '$';
   pos = str.find_first_of(target, pos + 1);
  }
  cout << str << endl;

  // Also: string::size_type pos = str.find_first_not_of(target); can be used
  // to find chars that are not from what was specified

  // Also: string::size_type pos = str.find_last_of(target); can be used
  // to find chars from the end of the string

  // Also: string::size_type pos = str.find_last_not_of(target); can be used
  // to find chars that are not from what was speficified on target starting
  // from the end of the string

  return 0;
}
