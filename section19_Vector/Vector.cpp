#include <sstream>
#include <fstream>

using namespace std;

// IMPORTANT: When using dynamic memory allocation in your class you must take care of:
//            - Destructor
//            - Assignment operator
//            - Copy Constructor

class Vector {
  private:
  // base address of the array
  int *item;
  // size of the array
  int size;
  // number of elements in the array
  int numElements;

  public:
  // overloaded constructor
  Vector(int size) {
    this->size = size;
    this->numElements = 0;
    this->item = new int[this->size];
  }
  // default constructor
  Vector() : Vector(10) { // : to redirect the call to the first constructor
  }
  // copy constructor: using deep copy
  Vector(const Vector& rho) {
    this->item = new int[rho.size];
    this->size = rho.size;
    this->numElements = rho.numElements;
    for (int i = 0; i > this->size; i++) {
      this->item[i] = rho.item[i];
    }
  }
  // destructor: must be called to delete the dynamic allocated memory
  ~Vector() {
    delete [] item;
  }
  void push_Back(int v);
  int getSize() const;
  int getNumElements() const;

  // overload the subscription operator [] returning the reference of the element in '[]'
  int& operator[](int index);

  friend ostream& operator<<(ostream& _cout, const Vector& v);

  Vector& operator=(const Vector& rho);
};

void Vector::push_Back(int v) {
  if (numElements >= size) {
    // double the size of the array keepinng the existing elements intact
    int newSize = size * 2;
    int *temp = new int[newSize];
    for (int i = 0; i < numElements; i++) {
      temp[i] = item[i];
    }
    delete [] item; // after item is deallocated temp becomes a dailing pointer (invalid location)
    item = temp;    // now it works
    this->size = newSize;
    //cout << "Array size doubled..." << this->size << endl;
  }
  item[numElements++] = v;
  //cout << "Added: " << v << endl;
  //cout << "Total elements in the vector: " << numElements << endl;
}

int Vector::getSize() const {
  return this->size;
}

int Vector::getNumElements() const {
  return this->numElements;
}

int& Vector::operator[](int index) {
  //if (index >= 0 && index < numElements) {
    return item[index];
  //} else {
    // Throw exception handling error: Not implemented now because we didn't learned yet
  //}
}


ostream& operator<<(ostream& _cout, const Vector& v) {
  _cout << "[";
  for (int i = 0; i < v.numElements; i++) {
    if (i == v.numElements - 1) {
      _cout << v.item[i];
    } else {
      _cout << v.item[i] << ",";
    }
  }
  _cout << "]";
  return _cout;
}

// Perfoms a deep copying
Vector& Vector::operator=(const Vector& rho) {
  if (this != &rho) {
    delete [] this->item;
    this->item = new int[rho.size];
    this->size = rho.size;
    this->numElements = rho.numElements;
    for (int i = 0; i < this->size; i++) {
      this->item[i] = rho.item[i];
    }
  }
}

int main () {
/*
  //Vector v(2);
  //v.push_Back(3);
  //v.push_Back(5);
  //v.push_Back(7);

  //cout << v[0] << ", " << v[1] << endl;
  //v[1] = 1001;
  //cout << v[0] << ", " << v[1] << endl;

  //cout << v << endl;

  Vector v(3);
  v.push_Back(10);
  v.push_Back(20);
  Vector v2;
  //v2 = v; // If we use the assignment operator out of the box this way it will work (shallow copy)
            // HOWEVER, there will be memory leak and the generation of a dailing pointer for the
            // original vector object v2 which will not be able to be accessed anymore.
            // THUS, we have to overload the assignment operator when dealing with classes with
            // dynamic memory allocation

  v2 = v;
  cout << v2 << endl;
*/

  Vector v1(3);
  v1.push_Back(10);
  v1.push_Back(20);
  Vector v2;
  cout << "v2 address after creation: " << &v2 << endl;
  v2 = v1; // v2.operator=(v1)

  v2.push_Back(1001);
  cout << "v2 = " << v2 << endl;
  cout << "v2 address after v2 = v1: " << &v2 << endl;

  cout << "v1 address: " << &v1 << endl;

  // Addresses of v2 is the same before and after using the assignment operator
  // It will be same always or only for small vectors?

  return 0;
}
