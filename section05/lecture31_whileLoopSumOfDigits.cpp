#include <iostream>
#include <cmath>
// write a program to find the sum of te digits of a number

int main (int argc, char const *argv[]) {

  int n, rem {1}, sum {0};

  std::cout <<  "Enter any number (0 to terminate): ";
  std::cin >> n;
  if (n < 0) {
    n = abs(n);
  }

  while (rem != 0) {
    rem = n % 10;
    sum += rem;
    n /= 10;
  }
  std::cout  << "The sum of the digits is:  " << sum << std::endl;

  return 0;
}
