#include <iostream>
#include <cmath>

using namespace std;

// Operator overloading - redefining operator for objects.

class Circle {
  private:
  int radius;

  public:
  Circle() {
    radius = 0;
  }
  Circle(int radius) {
    this->radius = radius;
  }
  double getArea() {
    return 3.14159 * radius * radius;
  }
  void display() {
    cout << "Radius: " << radius
         << ", Area: " << getArea() << endl;
  }
  // Operator overload for '+'
  Circle operator+ (const Circle& rho) {
    Circle result;
    result.radius = this->radius + rho.radius;
    return result;
  }
  // Operator overload for '-'
  Circle operator- (const Circle& rho) {
    Circle result;
    result.radius = abs(this->radius - rho.radius);
    return result;
  }

};

int main () {

  int a = 5, b = 6, c;
  c = a + b;

  // we cannot sum two circle objects because compiler doesnt know how to handle this operation
  Circle c1(5), c2(6), c3, c4, c5;
  c3 = c1 + c2; // c1.operator+(c2)
  c4 = c1 - c2; // c1.operator-(c2)
  c5 = c3 - c4 + c1 + c2;

  // we can redefine the '+' operador so it can work within our object scope
  c3.display();
  c4.display();
  c5.display();

  return 0;
}

