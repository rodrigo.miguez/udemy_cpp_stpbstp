#include <iostream>

int main (int argc, char const *argv[]) {

  int array[100];
  int n;
  
  std::cout << "Input how many numbers: ";
  std::cin >> n;

  while (n <= 0 || n > 100) {
    std::cout << "Invalid input, valid range is > 0 and <= 100." << std::endl;
    std::cout << "Input again: ";
    std::cin >> n;
  }
  for (int i = 0; i < n; ++i) {
    std::cout << "Input value for index: " << i << ": ";
    std::cin >> array[i];
  }
  // printing back into the console
  std::cout << "Content of the array: " << std::endl;
  for (int i = 0; i < n; ++i) {
    std::cout << array[i] << " ";
  }
  std::cout << "\n";

  return 0;
}
