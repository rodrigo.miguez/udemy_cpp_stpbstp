#include <iostream>
#include <string>

using namespace std;

int main () {

  string str = "Hello";
/*
  string::iterator it = str.begin(); // it is placed at H

  while(it != str.end()) {
    cout << *it;
    // Convert lower to upper case:
    if (*it >= 'a' && *it <= 'z') {
      *it -= 32;
    }
    it++;
  }
  cout << endl;
  cout << str << endl;
*/

  // For the reverse operator:
  string::reverse_iterator rit = str.rbegin(); // it is placed at o
  while(rit != str.rend()) {
    cout << *rit;
    // Convert lower to upper case:
    if (*rit >= 'a' && *rit <= 'z') {
      *rit -= 32;
    }
    rit++;
  }
  cout << endl;
  cout << str << endl;

  return 0;
}
