#include <iostream>
#include <cmath>

using namespace std;

// Operator overloading - redefining operator for objects.

class Circle {
  private:
  int radius;

  public:
  Circle() {
    radius = 0;
  }
  Circle(int radius) {
    this->radius = radius;
  }
  double getArea() {
    return 3.14159 * radius * radius;
  }
  void display() {
    cout << "Radius: " << radius
         << ", Area: " << getArea() << endl;
  }
  // Operator overload for '+'
  Circle operator+ (const Circle& rho) {
    Circle result;
    result.radius = this->radius + rho.radius;
    return result;
  }
  // Operator overload for '-'
  Circle operator- (const Circle& rho) {
    Circle result;
    result.radius = abs(this->radius - rho.radius);
    return result;
  }
  Circle operator++() {
    // prefix
    this->radius++;
    Circle result;
    result.radius = this->radius;
    return result;
  }
  Circle operator++(int) { // in C++ we assgin postfix by receiving a dummy paramenter at the
    // postfix                operator overload method (language recomendation). It simply tells
    //                        the compiler that this is the postfix version.
    Circle result;
    result.radius = this->radius;
    this->radius++;
    return result;
  }
  Circle operator--() {
    // prefix
    this->radius--;
    Circle result;
    result.radius = this->radius;
    return result;
  }
  Circle operator--(int) { // in C++ we assgin postfix by receiving a dummy paramenter at the
    // postfix                operator overload method (language recomendation). It simply tells
    //                        the compiler that this is the postfix version.
    Circle result;
    result.radius = this->radius;
    this->radius--;
    return result;
  }

};

int main () {

  Circle c1(5);
  Circle c;
  //c = ++c1; // prefix unary increment: c1 will be incremented first (c(6), c1(6))
  //c = c1++; // postfix unary increment: c will be assigned to the value of c1 (c(5), c1(6))

  //++c1; // c1.operator++();
  //c1++; // c1.operator++(int);

  c = ++c1;
  c1.display();
  c.display();
  cout << endl;

  c = c1++;
  c1.display();
  c.display();
  cout << endl;

  c = --c1;
  c1.display();
  c.display();
  cout << endl;

  c = c1--;
  c1.display();
  c.display();

  return 0;
}

