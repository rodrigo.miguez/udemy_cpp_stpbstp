#include <iostream>

int main (int argc, char const *argv[]) {

  for (int i = 0; i < 100; ++i) {
    std::cout << i << std::endl;
    if (i % 5 == 0 && i != 0) {
      break;
    }
  }

  return 0;
}
