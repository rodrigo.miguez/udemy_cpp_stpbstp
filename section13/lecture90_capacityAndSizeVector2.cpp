#include <iostream>
#include <vector>

using namespace std;

int main () {

  vector <string> v1;
  cout << "Capacity: " << v1.capacity()
       << ", Size: " << v1.size() << endl;

  v1.push_back("Larissa");
  cout << "Capacity: " << v1.capacity()
       << ", Size: " << v1.size() << endl;

  v1.push_back("Rodrigo");
  cout << "Capacity: " << v1.capacity()
       << ", Size: " << v1.size() << endl;

  v1.push_back("Marlene");
  v1.push_back("Ricardo");
  v1.push_back("Karla");
  cout << "Capacity: " << v1.capacity()
       << ", Size: " << v1.size() << endl;

  v1.shrink_to_fit();
  cout << "Capacity: " << v1.capacity()
       << ", Size: " << v1.size() << endl;

  return 0;
}
