#include <iostream>

int main(int argc, char* argv[])
{

//  int var {}; // The empity curly braces means we are assigning zero to the variable var
  int var;      // By default the variable is allocated somewhere in memory and
                // whatever the content of this memory location is will be the
                //  value of the variable: called garbage value

  std::cout << "var = " << var << std::endl;

  std::cout << "Enter an integer value: ";
  // To receive input from the user's keyboard we use the "cin" object
  std::cin >> var;
              // This time the operator (extractor operator) is taking the
              // object from the keyboard input and transmitting it to the
              // memory (extract it to a variable). It extracts the value
              // according to ts type automatically

  std::cout << "The value of var is: " << var << std::endl;

  return 0;
}
