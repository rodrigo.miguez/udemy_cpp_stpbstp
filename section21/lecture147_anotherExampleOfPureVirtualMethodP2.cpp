#include <iostream>

using namespace std;

class Flyable { // Inteface type (pure abstract type)
  public:
  virtual void startFlight() = 0;
  virtual void stopFlight() = 0;
};

class Bird {
  protected:
  string name;

  public:
  Bird(string name) {
    this->name = name;
  }
  string getName() {
    return this->name;
  }
};

// Multiple inheritance
class Eagle:public Bird, public Flyable {
  private:
  string masterName;

  public:
  Eagle(string name, string masterName):Bird(name) {
    this->masterName = masterName;
  }
  void startFlight() {
    cout << "Eagle " << Bird::getName()
         << ", started flight in its own way" << endl;
  }
  void stopFlight() {
    cout << "Eagle " << Bird::getName()
         << ", stopped flight in its own way" << endl;
  }
};

// Multiple inheritance
class Owl:public Bird, public Flyable {
  private:
  int nightVisionPower;

  public:
  Owl(string name, int nightVisionPower):Bird(name) {
    this->nightVisionPower = nightVisionPower;
  }
  void startFlight() {
    cout << "Owl " << Bird::getName()
         << ", started flight at low height" << endl;
  }
  void stopFlight() {
    cout << "Owl " << Bird::getName()
         << ", stopped flight on a tree" << endl;
  }

};

class Airplane:public Flyable {
  private:
  string make;

  public:
  Airplane(string make) {
    this->make = make;
  }
  void startFlight() {
    cout << "Airplane " << make
         << " started its flight" << endl;
  }
  void stopFlight() {
    cout << "Airplane " << make
         << " stopped its flight" << endl;
  }

};

class Game {
  // Here we see again the dynamic polymorphism in action:
  // The pointer is created w.r.t. the base class Bird and we assigne the subtype objects in each
  // element of the birds[] array. Then we call the start or stop flight methods using the super
  // type pointers so the compiler looks into the pure virtual method during the compiled time and
  // during the execution time they are overwritten by the method in the subtype objects. which hets
  // the call.
  private:
  //Bird* birds[3];
  Flyable* flyables[5];

  public:
  Game() {
    flyables[0] = new Eagle("Super Falcon", "John");
    flyables[1] = new Owl("Night Captain", 10);
    flyables[2] = new Eagle("Knight", "Alice");
    flyables[3] = new Airplane("Boeing");
    flyables[4] = new Airplane("Airbus");
  }
  ~Game() {
    for (int i = 0; i < 5; i++) {
      delete flyables[i];
    }
  }
  void playGame() {
    for (int i = 0; i < 5; i++) {
      flyables[i]->startFlight(); // dynamic polymorphism happening
    }
    cout << endl;
    cout << "All flyables are flying around the game arena" << endl;
    cout << endl;
    for (int i = 0; i < 5; i++) {
      flyables[i]->stopFlight();
    }
  }
};

int main () {

  Game game;
  game.playGame();

  return 0;
}

