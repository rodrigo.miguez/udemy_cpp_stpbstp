#include <iostream>
#include <string>

using namespace std;

class Class {
  public:
  virtual int atk() = 0;
  template<int N, class ClassType> friend class totalHitPoints;
};

class Archer:public Class {
  private:
  int force = 8;
  string name = "Archer";

  public:
  int atk() {
    return force + 1;
  }
  string getClassName() {
    return name;
  }
};

class Warrior:public Class {
  private:
  int force = 10;
  string name = "Warrior";

  public:
  int atk() {
    return force + 2;
  }
  string getClassName() {
    return name;
  }
};

template<int N, class ClassType>
class totalHitPoints {
  private:
  double value;

  public:
  totalHitPoints(double value) {
    //cout << __PRETTY_FUNCTION__ << endl;
    this->value = value;
  }
  void performAtkAction() {
    ClassType c;
    cout << "The amount of damage dealt from the " << c.getClassName() << " is equal to "
         << N + value + c.atk() << " hit points." << endl;
  }
};

int main () {

  totalHitPoints<3, Warrior> character(1);
  character.performAtkAction();

  /*
  Class *p = new Archer();
  cout << p->atk() << endl;
  */

  return 0;
}

