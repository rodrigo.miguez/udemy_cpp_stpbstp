#include <iostream>

using namespace std;

int main () {

    // --- DO NOT CHANGE ABOVE THIS LINE. ---
    // You will need to test for the valid n. Read the instructions carefully and proceed.
    // You should write your code after this line.
    int n {0};

    cout << "Enter a number: ";
    cin >> n;

    if (n <= 0) {
        std::cout << "INVALID LINE NUMBERS." << std::endl;
        return EXIT_FAILURE;
    }

    for (int i = 0; i < n; ++i) {
        for (int j = 0; j <= i; ++j) {
            cout << "*";
        }
        cout << endl;
    }


}
