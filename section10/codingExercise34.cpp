#include <iostream>
#include <sstream>
#include <string>

using namespace std;

int main() {

  int array[] = {1,2,3,4,5,8,10};
  int n = sizeof(array)/sizeof(int);

  cout << "n = " << n << endl;

  ostringstream oss;
  if (n == 0 || (n == 1 && array[0] % 2 != 0)) {
    cout << "$";
  } else {
    for (int i = 0; i < n; i++) {
      if (array[i] % 2 == 0) {
        oss << array[i] << "$";
      }
    }
  }

  string str = oss.str();
  str.erase(str.end()-1);
  cout << str << endl;

  return 0;
}
