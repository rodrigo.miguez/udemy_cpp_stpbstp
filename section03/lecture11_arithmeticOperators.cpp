#include <iostream>

int main(int argc, char* argv[])
{

  int first {20}, second {10}, result;

  std::cout << "first = " << first << ", second = " << second << std::endl;

  result = first + second;
  std::cout << "Result = first + second = " << result << std::endl;

  result = first - second;
  std::cout << "Result = first - second = " << result << std::endl;

  result = first * second;
  std::cout << "Result = first * second = " << result << std::endl;

  result = first / second;
  std::cout << "Result = first / second = " << result << std::endl;

  result = first % second;
  std::cout << "Result = first % second = " << result << std::endl;
  // % is used to find the remainder if we devide one value by another
  // usually used to check if values are divisible

  result = (first + 5) % second;
  std::cout << "Result = (first + 5) % second = " << result << std::endl;

  return 0;
}
