#include <iostream>

// Passing pointers to a function
void fun(int* value1, int* value2) {
  // Here value1 and value2 are memory addresses, so we need to use the dereference operator:
  *value1 = *value1 + 10;
  std::cout << "Value1 = " << *value1 << std::endl;
  
  *value2 = *value2 + 5;
  std::cout << "Value2 = " << *value2 << std::endl;
}

int main(int argx, char const *argv[]) {
  
  std::cout << "Passing pointers to a function!" << std::endl;

  int a {10}, b {20};

  fun(&a, &b);
  
  std::cout << "\n";
  std::cout << "a = " << a << std::endl;
  std::cout << "b = " << b << std::endl;

  return 0;
}
