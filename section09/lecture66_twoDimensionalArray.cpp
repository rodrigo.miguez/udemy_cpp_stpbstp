#include <iostream>

void print2DArray(int arr[][100], int rows, int cols) {
  std::cout << "{{";
  for (int i = 0; i < rows; ++i) {
    for (int j = 0; j < cols; ++j) {
      if (i != 0 && j == 0) {std::cout << "  ";}
      std::cout <<  arr[i][j];
      if (j < cols-1) {std::cout << ", ";}
    }
  if (i < rows-1) {std::cout << ";\n";}
}
  std::cout << "}}\n";
}

int main() {

  const int ROWS {100};
  const int COLS {100};

  int arr[ROWS][COLS];
  int nRows, nCols;

  std::cout << "How many rows: ";
  std::cin >> nRows;

  while (nRows <= 0 || nRows > ROWS) {
    std::cout << "Invalid number of rows, valid is > 0 and <= " << ROWS << std::endl;
    std::cout << "Please input again: ";
    std::cin >> nRows;
  }

  std::cout << "How many columns: ";
  std::cin >> nCols;

  while (nCols <= 0 || nCols > COLS) {
    std::cout << "Invalid number of columns, valid is > 0 and <= " << COLS << std::endl;
    std::cout << "Please input again: ";
    std::cin >> nCols;
  }

  for (int i = 0; i < nRows; ++i) {
    for (int j = 0; j < nCols; ++j) {
      std::cout << "Row index: " << i << ", Column index: " << j << ": ";
      std::cin >> arr[i][j];
    }
  }

  std::cout << "Content of the 2D array: \n";
  print2DArray(arr, nRows, nCols);

  return 0;
}
