#include <iostream>

int main(int argc, char* argv[])
{

  int x {10};

  std::cout << "x = " << x << std::endl;
  x--; // Unary decrement operator in a postfixed manner (for prefixed: --x;)
  std::cout << "After decrement: x = " << x << std::endl;
  x++; // Unary increment operator in a postfixed manner (for prefixed: ++x;)
  std::cout << "After increment: x = " << x << std::endl;
  // Both postfixed and prefixed will have the same impact on the output

  // The difference between post and pre fixed comes when dealing with additional
  // operators, for example:
  int y {};
  std::cout << "y = " << y << std::endl;
  y = x++;
  std::cout << "postfixed (y = x++;): y = " << y << ", x = " << x << std::endl;
                      // It assigns the value of x to y first, and only then
                      // x is incremented by 1

  y = ++x;
  std::cout << "prefixed (y = ++x;): y = " << y << ", x = " << x << std::endl;
                      // It increments the value of x and then assigns the value
                      // of x to y

  return 0;
}
