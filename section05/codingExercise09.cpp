#include <iostream>

using namespace std;

int main () {   // n will be passed by auto tester
    // --- Example output:

    //     Output1:

    //     When n = 46712, your program should print the following exactly
    //     Total even digits = 3
    //     Total odd digits = 2
    //     The number is a MANGO number.
    // (a new line must be appended at the end of each line your program prints)
    // *** NOTICE the full stop at the end of the last line and after that there should be a new line.

    //     Output2:
    //     When n = 0, your program should print the following exactly
    //     Total even digits = 1
    //     Total odd digits = 0
    //     The number is a MANGO number.
    // (a new line must be appended at the end of each line your program prints)
    // *** NOTICE the full stop at the end of the last line and after that there should be a new line.

    //     Output3:
    //     When n = 1, your program should print the following exactly
    //     Total even digits = 0
    //     Total odd digits = 1
    //     The number is a ORANGE number.
    // (a new line must be appended at the end of each line your program prints)
    // *** NOTICE the full stop at the end of the last line and after that there should be a new line.

    //     Output4:
    //     When n = 334, your program should print the following exactly
    //     Total even digits = 1
    //     Total odd digits = 2
    //     The number is a ORANGE number.
    // (a new line must be appended at the end of each line your program prints)
    // *** NOTICE the full stop at the end of the last line and after that there should be a new line.

    //     Output5:
    //     When n = 3348, your program should print the following exactly
    //     Total even digits = 2
    //     Total odd digits = 2
    //     The number is a GUAVA number.
    // (a new line must be appended at the end of each line your program prints)
    // *** NOTICE the full stop at the end of the last line and after that there should be a new line.

    //     Output6:
    //     When n = 600, your program should print the following exactly
    //     Total even digits = 3
    //     Total odd digits = 0
    //     The number is a MANGO number.
    // (a new line must be appended at the end of each line your program prints)
    // *** NOTICE the full stop at the end of the last line and after that there should be a new line.
    // --- PLEASSE DO NOT CHANGE ABOVE THIS LINE ---
    // Write your code after this line.
    int n {0}, rem {0}, evenNum {0}, oddNum {0};
    string name;

    cout << "Enter a number: ";
    cin >> n;

    do {
      rem = n % 10;
      (rem == 0)?++evenNum:((rem % 2 == 0)?++evenNum:++oddNum);
      n /= 10;
    } while (n != 0);

    if (evenNum == oddNum) {
      name = "GUAVA";
    } else if (evenNum > oddNum) {
      name = "MANGO";
    } else {
      name = "ORANGE";
    }

    cout << "Total even digits = " << evenNum << "\n"
         << "Total odd digits = " << oddNum << "\n"
         << "The number is a " << name << " number." << endl;

}
