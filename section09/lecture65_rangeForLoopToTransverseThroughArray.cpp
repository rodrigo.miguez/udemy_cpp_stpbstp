#include <iostream>

const int size_of_array = 5;


// To use the range for loop in a function we need to pass the array as a reference because we
// provide the size of the array this way. Range for loop can only work if it know the size of the
// array to be fetched.
void printArray(int (&p)[size_of_array]) {
  std::cout << "In the function:\n";
  for (auto k:p) {
    std::cout << k << " ";
  }
  std::cout << "\n";
}

int main(int argc, char const *argv[]) {

  int x[] = {10, 20, 30, 40, 50};

  /*
  for (int i = 0; i < size_of_array; ++i) {
    std::cout << x[i] << " ";
  }
  std::cout << "\n";
  */

  // Using the range for:
  // Here p will assume each one of the elements in x.
  // "auto" type can be used (instead of int in this case) if you are not sure about the data type
  // of the array.
  // To modify the elements of the array we have to use: &p.
  for (auto &p:x) {
    //std::cout << p << " ";
    p *= 2;
  }
  /*
  for (auto k:x) {
    std::cout << k << " ";
  }
  std::cout << "\n";
  */
  printArray(x);

  return 0;
}
