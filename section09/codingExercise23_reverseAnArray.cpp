#include <iostream>

using namespace std;

void printArray(int *arr, int n) {
  for (int i = 0; i < n; ++i) {
    std::cout << *(arr + i) << " ";
  }
  std::cout << "\n";
}

void reverseArr(int arr[], int size){
    int buffer;
    for (int i = 0, j = size-1; i < j; ++i, --j) {
        buffer = arr[i];
        arr[i] = arr[j];
        arr[j] = buffer;
    }
}

int main(int argc, char const *argv[]) {

  int a[6] = {10, 20, 30, 40, 50, 60};
  std::cout << "The array is: \n";
  printArray(a, 6);

  std::cout << "The reverse array is: \n";
  reverseArr(a, 6);
  printArray(a, 6);

  return 0;
}
