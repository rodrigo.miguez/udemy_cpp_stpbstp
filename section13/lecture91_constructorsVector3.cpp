#include <iostream>
#include <vector>

using namespace std;

void display(vector <int> &v) {
  cout << "Size: " << v.size() << ", Capacity: " << v.capacity() << endl;
  cout << "Content of the vector: " << endl;
  for (int p:v) {
    cout << p << ", ";
  }
  cout << endl;
}

int main () {

  vector <string> v1; // empty vector
  vector <int> v2(5); // 5 elements located with zeros

  display(v2);
  cout << endl;

  vector <int> v3(5, -1); // 5 elements initialized with -1
  display(v3);
  cout << endl;

  int x[] = {10, 20, 30, 40, 50};
  vector <int> v4 (x, x+5); // give all elements of x
  display(v4);
  cout << endl;

  vector <int> v5 (x, x+3); // give first three elements of x
  display(v5);
  cout << endl;

  vector <int> v6 (x+1, x+3); // from second to third element
  display(v6);
  cout << endl;

  vector <int> v7 (v6); // copy content of v6 t v7
  display(v7);
  cout << endl;

  // Check now that v6 and v7 are independet
  v6.push_back(40);
  display(v6);
  cout << endl;

  display(v7);
  cout << endl;

  vector <int> v8 (v4.begin(), v4.begin()+3);
  display(v8);
  cout << endl;

  vector <int> v9 (v4.begin(), v4.end()); // copy entire vector
  display(v9);
  cout << endl;

  vector <int> v10 (v4.rbegin(), v4.rend()); // copy in the reverse order
  display(v10);
  cout << endl;

  return 0;
}
