#include <iostream>
#include <sstream>
#include <string>

using namespace std;

int main() {

  string name = "John";
  int age = 10; 

  ostringstream oss;

  oss << name << ":" << age;
  string str = oss.str();
  cout << str << endl;

  return 0;
}
