#include <iostream>

int main (int argc, char const *argv[]) {

  int array[10];
  array[0] = 5;
  array[1] = 10;
  array[9] = array[0] + array[1];
  std::cout << "array[9] = " << array[9] << std::endl;

  // The name of the array always giver the address of the first member of the array
  std::cout << std::endl;
  std::cout << "Address of array[0] = " << (unsigned long long)&array[0] << std::endl;
  std::cout << "Address of array[1] = " << (unsigned long long)&array[1] << std::endl;
  std::cout << "Address of array[9] = " << (unsigned long long)&array[9] << std::endl;
  std::cout << "Address of array    = " << (unsigned long long)&array    << std::endl;

  return 0;
}
