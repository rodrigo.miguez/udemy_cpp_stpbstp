#include <iostream>
#include <cmath>
#include <fstream>

using namespace std;

// Operator overloading - redefining operator for objects.

class Circle {
  private:
  int radius;

  public:
  Circle() {
    radius = 0;
  }
  Circle(int radius) {
    this->radius = radius;
  }
  double getArea() const {
    return 3.14159 * radius * radius;
  }
  void display() {
    cout << "Radius: " << radius
         << ", Area: " << getArea() << endl;
  }
  // Operator overload for '+'
  Circle operator+ (const Circle& rho) {
    Circle result;
    result.radius = this->radius + rho.radius;
    return result;
  }
  // Operator overload for '-'
  Circle operator- (const Circle& rho) {
    Circle result;
    result.radius = abs(this->radius - rho.radius);
    return result;
  }
  Circle operator++() {
    // prefix
    this->radius++;
    Circle result;
    result.radius = this->radius;
    return result;
  }
  Circle operator++(int) { // in C++ we assgin postfix by receiving a dummy paramenter at the
    // postfix                operator overload method (language recomendation). It simply tells
    //                        the compiler that this is the postfix version.
    Circle result;
    result.radius = this->radius;
    this->radius++;
    return result;
  }
  Circle operator--() {
    // prefix
    this->radius--;
    Circle result;
    result.radius = this->radius;
    return result;
  }
  Circle operator--(int) { // in C++ we assgin postfix by receiving a dummy paramenter at the
    // postfix                operator overload method (language recomendation). It simply tells
    //                        the compiler that this is the postfix version.
    Circle result;
    result.radius = this->radius;
    this->radius--;
    return result;
  }

  friend ostream& operator<<(ostream& _cout, const Circle& c);
  friend istream& operator>>(istream& _cin, Circle& c);

  bool operator>(const Circle& rho) {
    return (this->radius > rho.radius);
  }
  bool operator<(const Circle& rho) {
    return (this->radius < rho.radius);
  }
  bool operator==(const Circle& rho) {
    return (this->radius == rho.radius);
  }
  bool operator!=(const Circle& rho) {
    return (this->radius == rho.radius);
  }
  bool operator>=(const Circle& rho) {
    return (this->radius >= rho.radius);
  }
  bool operator<=(const Circle& rho) {
    return (this->radius <= rho.radius);
  }
};

ostream& operator<<(ostream& _cout, const Circle& c) {
  _cout << "Radius: " << c.radius
        << ", Area: " << c.getArea() << endl;
  return _cout;
}

istream& operator>>(istream& _cin, Circle& c) {
  _cin >> c.radius;
  return _cin;
}

int main () {

  Circle c1(5), c2(6);
  // >, <, ==, !=, >=, <=


  if (c1 > c2) { // c1.operator>(c2);
    cout << "c1 > c2" << endl;
  } else {
    cout << "c1 < c2" << endl;
  }

  return 0;
}

