#include <iostream>

using namespace std;

class Employee {
  protected:
  string name;
  int age;
  double basic;

  public:
  Employee(string name, int age, double basic) {
    this->name = name;
    this->age = age;
    this->basic = basic;
  }
  // If every employee has salary but there is no common way of defining it
  // Then we can define a pure virtual method as follows:
  virtual double calculateSalary() = 0; // now the overwrite of calculaSalary in the subtype is a
                                        // must because it is incompleted here. The employee class is
                                        // now called an abstract type
};

class Manager: public Employee {
  private:
  double hra;

  public:
  Manager(string name, int age, double basic):Employee(name, age, basic) {
    hra = basic * 0.15;
  }
  double calculateSalary() {
    double salary = basic * 0.15;
    salary += hra;
    return salary;
  }
};

class FactoryWorker: public Employee {
  private:
  double tiffinAllowences;

  public:
  FactoryWorker(string name, int age, double basic):Employee(name, age, basic) {
    tiffinAllowences = 0.05 * basic;
  }
  double calculateSalary() {
    double salary = basic * 0.08;
    salary += tiffinAllowences;
    return salary;
  }
};

class Clerk: public Employee {
  private:
  int otHours;
  double otRate;

  public:
  Clerk(string name, int age, double basic, int otHours, double otRate):Employee(name, age, basic) {
    this->otHours = otHours;
    this->otRate = otRate;
  }
  double calculateSalary() {
    double salary = basic * 0.1;
    salary += otRate * otHours;
    return salary;
  }

};

class Club { // Club class is general
  public:
  string getMembershipType(Employee *p) { // if we use type object it will work for any Employee subtype
                                          // this way we dont need to cread a method for each
                                          // employee subtype
    double salary = p->calculateSalary(); // Dynamic Polymorphism:
                                          // In compilation time the compiler will see that the
                                          // pointer is pointing to an Employee object
                                          // It works because we have the virtual calculateSalary()
    if (salary >= 10000.0) {
      return "Red";
    } else if (salary >= 7500.0) {
      return "Blue";
    } else {
      return "Grey";
    }
  }
};

int main () {

  /*
  Manager *p = new Manager("Alice",25, 8000.0); // Employee *p = new Manager(...); is possible
  double salary = p->calculateSalary(); // .calculateSalary() doesnt work in this context
  cout << salary << endl;
  */

  /*
  Manager p("Alice",25, 8000.0);
  double salary = p.calculateSalary();
  cout << salary << endl;
  */

  // Test of the Club class
  Club club;
  cout << "Alice has " << club.getMembershipType(new Manager("Alice", 25, 8000.0))
       << " membership." << endl;
  cout << "John has "  << club.getMembershipType(new Clerk("John", 28, 5000.0, 10, 12.5))
       << " membership." << endl;

  return 0;
}

