#include <iostream>
#include <vector>

using namespace std;

void display(vector <string> &v) {
  cout << "Size: " << v.size() << ", Capacity: " << v.capacity() << endl;
  cout << "Content of the vector: " << endl;
  for (vector<string>::iterator it = v.begin(); it != v.end(); it++) {
    cout << *it << ", ";
  }
  cout << endl;
}

int main () {

  vector <string> names {"Larissa", "Rodrigo", "Ricardo", "Karla", "Marlene", "Lio", "Nico", "Stefan"};
  display(names);
  cout << endl;

  vector <string> names2 (names);

  names.front() = "AAA";
  display(names);
  cout << endl;

  names.back() = "BBB";
  display(names);
  cout << endl;

  names.erase(names.begin()); // erase the first element of the vector
  display(names);
  cout << endl;

  names.erase(names.end()); // erase the last element of the vector
  display(names);
  cout << endl;

  names2.erase(names2.begin() + names2.size()-2, names2.end()); // erase in a range
  display(names2);
  cout << endl;

  names2.insert(names2.begin(), "WWW"); // insert at the beginning
  display(names2);
  cout << endl;

  names2.insert(names2.end(), "YYY"); // insert at the end - Same can be done for any position
  display(names2);
  cout << endl;

  names2.pop_back(); // delete the last element from the vector
  display(names2);
  cout << endl;

  names2.clear(); // delete all elements from the vector
  display(names2);
  cout << endl;

  return 0;
}
