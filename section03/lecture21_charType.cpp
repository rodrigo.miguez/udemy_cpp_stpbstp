#include <iostream>

int main(int argc, char* argv[])
{

  char ch = 'a'; // A single charater value should be represented using single
                 // quotes

  std::cout << "ch = " << ch << ", ASCII value: " << (int)ch << std::endl;

  /*
  'A' = 65
  'B' = 66
  'C' = 67

  'a' = 97
  'b' = 98
  'c' = 99

  // To go from upper case to lower case: add 32 from the ASCII value
     To go from lower case to upper case: subtract 32 from the ASCII value

  */

  std::cout << "(ch-32) ch = " << char(ch-32)
            << ", ASCII value: " << (int)ch-32 << std::endl;

  return 0;
}
