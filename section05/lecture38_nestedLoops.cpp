#include <iostream>
// program to print the Fibonnaci seires

int main (int argc, char const *argv[]) {

  int n {0};
  std::cout << "Enter the number of terms: ";
  std::cin >> n;

  for (int i = 0; i < n; ++i) {
    for (int j = 0; j < n; ++j) {
      std::cout << "i = " << i << ", j = " << j << std::endl;
    }
  }

  return 0;
}
