#include <iostream>

int main (int argc, char const *argv[]) {

  int n, sum {0};
  char response;


  if (n < 0) {
    n = abs(n);
  }

  do {
    std::cout <<  "Enter a number: ";
    std::cin >> n;
    sum += n;

    std::cout << "Do you want to continue? Y/y to continue, any other to exit: ";
    std::cin >> response;
  } while (response == 'Y' || response == 'y');
  std::cout  << "The sum of the numbers is: " << sum << std::endl;

  return 0;
}
