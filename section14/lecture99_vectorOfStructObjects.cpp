#include <iostream>
#include <vector>

using namespace std;

struct Employee {
  int empId;
  string name;
  double salary;
};

void printEmployee(vector <Employee>& list) {
  cout << "Employee Data" << endl;
  cout << "----------------------" << endl;
  for (int i = 0; i < list.size(); i++) {
    Employee e = list.at(i); // or empList[i];
    cout << "Emp Id: " << e.empId << endl;
    cout << "Name: " << e.name << endl;
    cout << "Salary: " << e.salary << endl;
    cout << "----------------------" << endl;
  }
}

int main () {

  vector<Employee> empList;

  Employee e1 {1001, "John", 7500.0};
  Employee e2 {1002, "Terry", 5500.0};
  Employee e3 {1003, "Sovan", 4500.0};
  Employee e4 {1004, "Sree", 8750.0};

  empList.push_back(e1);
  empList.push_back(e2);
  empList.push_back(e3);
  empList.push_back(e4);

  printEmployee(empList);

  return 0;
}

