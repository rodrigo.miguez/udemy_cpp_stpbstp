#include <iostream>
#include <string>

using namespace std;

int main() {

  string str = "peace is my goal and program is peace";

  // To delete the whole string
  //str.clear();

  if(str.empty()) {
    cout << "String is empty!!" << endl;
  } else {
    cout << "String is NOT empty!!" << endl;
  }

  // To dele part of a string
  //str.erase(11, 5); // 12: index to start deleting; 4: number of chars to be deleted
  str.erase(str.begin()+12, str.end()-5);

  cout << str << endl;

  cout << endl;


  // Solution for Coding Exercise 31:
  string str1 = "0123456789";
  cout << str1 << endl;

  // Instruction1: erase the last character of the string str
  str1.erase(str1.end()-1);
  cout << "erase the last character of the string str" << endl;
  cout << str1 << endl;

  // Instruction2: erase the 5 characters starting from index 4
  str1.erase(4, 5);
  cout << "erase the 5 characters starting from index 4" << endl;
  cout << str1 << endl;

  // Instruction3: erase the second and third character
  str1.erase(1, 2);
  cout << "erase the second and third character" << endl;
  cout << str1 << endl;

  return 0;
}
