#include <iostream>
#include <sstream>
#include <string>

using namespace std;

class Car{

  public:
  //Constructors (overloading)
  Car();
  Car(string c);

  // Destructor
  ~Car();

  // Member methods
  void acceleration();
  void applyBreak();
  int getSpeed();
  void startEngine();
  void stopEngine();
  string toString();

  private:
  // Member variables
  string color;
  int speed;
  bool isEngineOn;

};

// Constructor Method
Car::Car() {
  cout << "Car object type instantiated..." << endl;
  color = "Gray";
  speed = 0;
  isEngineOn = false;
}

Car::Car(string c) {
  cout << "Car object type instantiated..." << endl;
  color = c;
  speed = 0;
  isEngineOn = false;
}

// Destructor Method
Car::~Car() {
  cout << "Car object type destroyed!" << endl;
}

void Car::acceleration() {
  if (isEngineOn) {
    speed += 10;
  }
}

void Car::applyBreak() {
  if (speed - 8 >= 0) {
    speed -= 8;
  } else {
    speed = 0;
  }
}

int Car::getSpeed() {
  return speed;
}

void Car::startEngine() {
  if (!isEngineOn) {
    isEngineOn = true;
  }
}

void Car::stopEngine() {
  if (isEngineOn) {
    isEngineOn = false;
    speed = 0;
  }
}

string Car::toString() {
  ostringstream oss;
  oss << "Color of the car: " << color << endl;
  oss << "Current speed: " << speed << endl;
  if (isEngineOn) {
    oss << "Engine is on" << endl;
  } else {
    oss << "Engine is off" << endl;
  }
  return oss.str();
}

int main () {

  Car car1, car2("Red");
  cout << car1.toString() << endl;
  car1.startEngine();
  cout << car1.toString() << endl;

  cout << car2.toString() << endl;
  car2.startEngine();

  car1.acceleration();
  car2.acceleration();
  car1.applyBreak();
  car1.applyBreak();

  cout << car1.toString() << endl;
  cout << car2.toString() << endl;

  return 0;
}
