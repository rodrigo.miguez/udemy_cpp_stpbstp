#include <iostream>
#include "codingExercise45.h"
using namespace std;
// The following function will return the distance object that contans the maximum distance
// in all the distances kept in vector v passed as parameter to this function.
Distance get_max_distance(vector<Distance> v){
    Distance max_distance;
    max_distance.feet = 0;
    max_distance.inches = 0.0;
    // Write your code to find maximum distance from all the distances kept in vector v
    // you will need to write a for loop to iterate through each of the Distance element in the vector v.
    // You can safely assume that all distances are > 0.

    // Hints: You may convert the distance into inches prior comparing and then it will be easier to compare
    for (int i = 0; i < v.size(); i++) {
        if (max_distance.inches <= v[i].feet) {
            max_distance.feet = v[i].feet;
            max_distance.inches = v[i].inches;
        }
    }


    return max_distance;
}

int main () {

	vector<Distance> V;

  Distance V0 {10, 120.0};
  Distance V1 {9, 108.0};
  Distance V2 {5, 60.0};
  Distance V3 {11, 132.0};
  Distance V4 {13, 156.0};
  Distance V5 {27, 324.0};
  Distance V6 {17, 204.0};
  Distance V7 {3, 36.0};
  Distance V8 {14, 168.0};
  Distance V9 {21, 252.0};

  V.push_back(V0);
  V.push_back(V1);
  V.push_back(V2);
  V.push_back(V3);
  V.push_back(V4);
  V.push_back(V5);
  V.push_back(V6);
  V.push_back(V7);
  V.push_back(V8);
  V.push_back(V9);

  Distance maxDistance = get_max_distance(V);

  cout << "Maximum Distance: " << maxDistance.feet << " [feet]" << endl
       << "Maximum Distance: " << maxDistance.inches << " [inches]" << endl;

  return 0;
}
