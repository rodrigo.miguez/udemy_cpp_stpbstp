#include <iostream>
#include <typeinfo>
#include <string>

using namespace std;

int main () {

  string str1 = "Welcome";
  cout << str1 << endl;
  cout << str1.length() << endl;
  cout << str1[str1.length()-2] << endl;
  cout << typeid(str1[str1.length()-2]).name() << endl;

  return 0;
}
