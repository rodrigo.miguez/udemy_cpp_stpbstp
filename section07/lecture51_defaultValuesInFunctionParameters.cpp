#include <iostream>
#include <cmath>

int add(int, int = 2); // The propotype of a function with a default value should be declared like
                       // this. Additionally, when using this propotype, the default value shoudn't
                       // be declared again at the function declaration otherwise there will be a
                       // compilation error.

// ADDITIONALLY: The default parameters should be at the end of the parameters list otherwise the
// compiler doesn't know with parameters receives the value being transmitted such as in add(10);

int add(int a, int b = 2) { // Usefull because b has a default parameter. Thus, the add function
                            // can be called using a single parameter add(10), or the two parameters
                            // add(10, 20) for example.

  return a + b;
}

int main (int argc, char const *argv[]) {

  int r = add(10);
  std::cout << r << std::endl;

  return 0;
}
