#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <cstdlib>
#include <string>
#include <climits>
#include <iomanip>

using namespace std;

const int EMP_ID_WIDTH = 8;
const int EMP_NAME_WIDTH = 20;
const int EMP_EMAIL_WIDTH = 25;
const int EMP_NUMERIC_WIDTH = 10;
const int TOTAL_WIDTH = 100;

struct Employee {
  int empId;
  string name;
  double basicSalary;
  double pf;
  double healthInsurance;
  string email;
};


// Function Prototypes
void readFile(vector<Employee>& v_emp);
void printMenu();
void doTask(vector<Employee>& v_emp, int option);
void addEmployee(vector<Employee>& v_emp);
int searchEmployee(vector<Employee> v_emp, int targetEmpId);
void saveToFile(vector<Employee> v_emp);
void printEmployees(vector<Employee> v_emp);
void printEmployee(Employee e);
double getNetSalary(Employee e);
bool deleteEmployee(vector<Employee>& v_emp, int targetEmpId);

void readFile(vector<Employee>& v_emp) {
  // Open file
  ifstream fin("emp.txt");
  if (!fin) {
    cout << "Unable to open database file emp.txt" << endl
         << "Make sure the file exists." << endl;
  }

  // Read file
  string line;
  int recNo = 0;
  while(!fin.eof()) {
    std::getline(fin, line);
    recNo++;

    // Now we parse each token for field values
    istringstream iss(line);
    string strEmpId;
    string strName;
    string strBasicSalary;
    string strPf;
    string strHealthInsurance;
    string strEmail;

    // Reads the line until finds a comma (field separator)
    std::getline(iss, strEmpId, ',');
    std::getline(iss, strName, ',');
    std::getline(iss, strBasicSalary, ',');
    std::getline(iss, strPf, ',');
    std::getline(iss, strHealthInsurance, ',');
    std::getline(iss, strEmail, ',');

    // Convert strings
    Employee temp;
    temp.empId = atoi(strEmpId.c_str()); // atoi used in legacy c string, so we need to convert
                                         // c++ string into legacy c string by adding ".c_str()"
    temp.name = strName;
    temp.basicSalary = atof(strBasicSalary.c_str());
    temp.pf = atof(strPf.c_str());
    temp.healthInsurance = atof(strHealthInsurance.c_str());
    temp.email = strEmail;

    // Store the information into the vector
    v_emp.push_back(temp);
  }
  fin.close();
}

void printMenu() {
  cout << "1. Add Employee" << endl;
  cout << "2. Print Employee Report" << endl;
  cout << "3. Search Employee" << endl;
  cout << "4. Delete Employee" << endl;
  cout << "5. Save" << endl;
  cout << "6. Exit" << endl;
}

void doTask(vector<Employee>& v_emp, int option) {
  int targetEmpId;
  int index;
  switch(option) {
    case 1: addEmployee(v_emp);
      break;
    case 2: printEmployees(v_emp);
      break;
    case 3:
      cout << "Enter employee id to search: ";
      cin >> targetEmpId;
      index = searchEmployee(v_emp, targetEmpId);
      if (index == -1) {
        cout << "Employee with id: " << targetEmpId << " does not exist" << endl;
      } else {
        cout << "Employee with id: " << targetEmpId << " found" << endl;
        printEmployee(v_emp[index]);
      }
      break;
    case 4:
      cout << "Enter employee id to search: ";
      cin >> targetEmpId;
      if (deleteEmployee(v_emp, targetEmpId)) {
        cout << "Employee with id: " << targetEmpId << " deleted successfully" << endl;
        cout << "Please use option - 5 to save the changes permanently" << endl;
      } else {
        cout << "Employee with id: " << targetEmpId << " could not be deleted" << endl;
      }
      break;
    case 5: saveToFile(v_emp);
      break;
    default: cout << "Invalid manu option chosen, valid options are from 1 - 6" << endl;
  }
}

void addEmployee(vector<Employee>& v_emp) {
  Employee temp;
  bool isDuplicate = false;
  do {
    cout << "Emp Id: ";
    cin >> temp.empId;
    isDuplicate = false;
    if (searchEmployee(v_emp,temp.empId) != -1) {
      isDuplicate = true;
      cout << "Employee id: " << temp.empId << " already exists, please input unique id." << endl;
    }
  } while(isDuplicate);
  cout << "Name: ";

  // Clear the input buffer to get temp.name using cin for std::getline
  cin.clear();
  cin.ignore(INT_MAX, '\n');
  std::getline(cin, temp.name);

  cout << "Basic Salary ($): ";
  cin >> temp.basicSalary;
  cout << "PF ($): ";
  cin >> temp.pf;
  cout << "Health Insurance ($): ";
  cin >> temp.healthInsurance;
  cout << "Email: ";

  // Clear the input buffer to get temp.email using cin for std::getline
  cin.clear();
  cin.ignore(INT_MAX, '\n');
  std::getline(cin, temp.email);

  // Store the information into the vector
  v_emp.push_back(temp);

  cout << "Employee with id: " << temp.empId << " added successfully" << endl;
  cout << "Total employees: " << v_emp.size() << endl << endl;
}

int searchEmployee(vector<Employee> v_emp, int targetEmpId) {
  for (int i = 0; i < v_emp.size(); i++) {
    if (v_emp[i].empId == targetEmpId) {
      return i;
    }
  }
  return -1;
}

void printEmployees(vector<Employee> v_emp) {
  cout << endl;
  // Print the heading
  cout << setw(EMP_ID_WIDTH) << left <<"EmpId"
       << setw(EMP_NAME_WIDTH) << left << "Name"
       << setw(EMP_EMAIL_WIDTH) << left << "Email"
       << setw(EMP_NUMERIC_WIDTH) << right << "Basic ($)"
       << setw(EMP_NUMERIC_WIDTH) << right << "PF ($)"
       << setw(EMP_NUMERIC_WIDTH) << right << "HltIns($)"
       << setw(EMP_NUMERIC_WIDTH) << right << "Net ($)"
       << endl;
  cout << setw(TOTAL_WIDTH) << setfill('-') << " " << endl;
  // Reset the setfill
  cout << setfill(' ');

  double totalBasic = 0.0;
  double totalPfDeduction = 0.0;
  double totalHealthInsurance = 0.0;
  double totalNetSalary = 0.0;

  for (vector<Employee>::iterator it = v_emp.begin(); it != v_emp.end(); it++) {
    printEmployee(*it);
    totalBasic += it->basicSalary;
    totalPfDeduction += it->pf;
    totalHealthInsurance += it->healthInsurance;
    totalNetSalary += getNetSalary(*it);
  }
  cout << setw(TOTAL_WIDTH) << setfill('-') << " " << endl;
  // Reset the setfill
  cout << setfill(' ');

  // Print the heading
  cout << setw(EMP_ID_WIDTH) << left <<"Total"
       << setw(EMP_NAME_WIDTH) << left << "In ($)"
       << setw(EMP_EMAIL_WIDTH) << left << " "
       << setw(EMP_NUMERIC_WIDTH) << setprecision(2) << right << fixed << totalBasic
       << setw(EMP_NUMERIC_WIDTH) << setprecision(2) << right << fixed << totalPfDeduction
       << setw(EMP_NUMERIC_WIDTH) << setprecision(2) << right << fixed << totalHealthInsurance
       << setw(EMP_NUMERIC_WIDTH) << setprecision(2) << right << fixed << totalNetSalary
       << endl;

}

void printEmployee(Employee e) {
  cout << setw(EMP_ID_WIDTH) << left << e.empId
       << setw(EMP_NAME_WIDTH) << left << e.name
       << setw(EMP_EMAIL_WIDTH) << left << e.email
       << setw(EMP_NUMERIC_WIDTH) << setprecision(2) << right << fixed << e.basicSalary
       << setw(EMP_NUMERIC_WIDTH) << setprecision(2) << right << fixed << e.pf
       << setw(EMP_NUMERIC_WIDTH) << setprecision(2) << right << fixed << e.healthInsurance
       << setw(EMP_NUMERIC_WIDTH) << setprecision(2) << right << fixed << getNetSalary(e)
       << endl;
}

double getNetSalary(Employee e) {
  return e.basicSalary - (e.pf + e.healthInsurance);
}

void saveToFile(vector<Employee> v_emp) {
  ofstream fout("emp.txt");
  if (!fout) {
    cout << "Unable to open the data file emp.txt" << endl;
    return;
  }
  int recCount = 0;
  for (vector<Employee>::iterator it = v_emp.begin(); it != v_emp.end(); it++) {
    fout << it->empId << "," << it->name << "," << it->basicSalary << ","
         << it->pf << "," << it->healthInsurance << "," << it->email;
    recCount++;
    if (recCount != v_emp.size()) {
      fout << endl;
    }
  }
  fout.close();
  cout << "Total of " << recCount << " records saved successfully into the file" << endl;
}

bool deleteEmployee(vector<Employee>& v_emp, int targetEmpId) {
  int index = searchEmployee(v_emp, targetEmpId);
  if (index == -1) {
    return false;
  }
  // record found at index
  cout << "Targe employee with id: " << targetEmpId << " foud" << endl;
  printEmployee(v_emp[index]);
  cout << "Are you sure to delete? Input 1 to delete 0 to exit" << endl;
  int option;
  cin >> option;
  if (option == 1) {
    v_emp.erase(v_emp.begin() + index);
    return true;
  }
  return false;
}

int main () {

  vector<Employee> v_emp;
  readFile(v_emp);

  cout << "Total " << v_emp.size() << " records read from the data file" << endl;

  printMenu();

  bool quit = false;
  while(!quit) {
    cout << "Input your option: ";
    int option;
    cin >> option;
    if (option == 6) {
      quit = true;
    } else {
      doTask(v_emp, option);
    }
  }

return 0;
}
