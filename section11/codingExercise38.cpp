#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <cstdlib>
#include <iomanip>
#include <typeinfo>

using namespace std;

/**
 *  This function will calculate the BMI of a person on supplying weight and height
 *  First parameter is the weight of the person in Kg
 *  Second parameter os the height of the person in meter.
 */ 
double getBMI(double weight, double height){
    return weight / (height * height);
}
/*
string getBMICmt(double BMI) {
    if (BMI < 18.5) {
		return "Underweight";
	} else if (BMI >= 18.5 && BMI < 25.0) {
		return "Normal";
	} else if (BMI >= 25.0 && BMI < 30.0) {
		return "Overweight";
	} else if (BMI >= 30.0) {
		return "Obase";
	}
}
*/
void filterTeensWithBMI(string source_file, string dest_file){
    ifstream fin(source_file);   // fin is the source file object.
    ofstream fout(dest_file);    // fout is the destination file object.
    // ----- please read the entire problem statement above before you start.  You code must meet each of the 
    // requirements.
    // first read the first line that contains the  number of records in the source file:
    int numRec = 0;
    fin >> numRec;
    
    string blank;
	getline(fin, blank);// *** reading the new line character at the end to move the file object at the beginning of 
                        // the second line, this is required as the file object will be before the newline claracter of the
                        // first line after reading that number of records, we need to move that file object to the second 
                        // line by reading that newline at the end of the first line.
                        
    // ----- write your code after this line. You will need a loop to iterate for numRec times
    // declare a variable to hold the count of underweight teens, initialize that with 0
    int age, count = 0;
	double weight, height, BMI;
	string name;
    string m_age;
    string m_weight;
    string m_height;
	string comment;
  	/*
	cout << setw(15) << left << "Name" << " "
		 << setw(7)  << right << "Age" << " "
       	 << setw(6)  << left <<  "Weight" << " "                                                                                                         
       	 << setw(9)  << left << "Height" << " "
		 << setw(7)  << left << "BMI" << endl;                                                                                                         
	cout << string(45, '-') << endl;
    */
	string line;
	for(int i = 1; i <= numRec; ++i){
        // read next line from input file using std::getline
        // parse the line, get the age weigh and height. You may use istringstream as I used in the video
        // if the age is teen age, calculate the BMI and then find the appropriate comment and 
        // finally write the record as instructed into the dest_file using fout. Remember, you will only
        // require to write the records of teen agers. Also do not forget to count the underweight teen agers
        getline(fin, line);
        istringstream iss(line);
        getline(iss, name, '$');                                                                                                                          
        getline(iss, m_age, '$');
		age = stoi(m_age.c_str());
        getline(iss, m_weight, '$');
		weight = stod(m_weight.c_str());
		getline(iss, m_height, '$');
		height = stod(m_height.c_str());
        BMI = getBMI(weight, height);
		//comment = getBMICmt(BMI);
    	if (BMI < 18.5) {
			comment = "Underweight";
		} else if (BMI >= 18.5 && BMI < 25.0) {
			comment = "Normal";
		} else if (BMI >= 25.0 && BMI < 30.0) {
			comment = "Overweight";
		} else if (BMI >= 30.0) {
			comment = "Obase";
		}

		
		/*
  		cout << setw(16) << left << name
		 	 << setw(7)  << right << age
       	 	 << setw(7)  << right << setprecision(2) << fixed << weight                                                                                                         
       	 	 << setw(7)  << right << setprecision(2) << fixed << height
		 	 << setw(7)  << right << setprecision(2) << fixed << BMI << endl;
		*/

		
		if (age >= 13 && age <= 19) {
			fout << name << "#" << age << "#"
				 << setprecision(2) << fixed << BMI << "#" << comment << endl;
			if (comment == "Underweight") {
				count++;
			}
		}
		
		
    }// end of for loop
    
    // write the count of underweight teen agers and then write a newline at the end.
	fout << count << endl;
    fin.close();
    fout.close();
}

int main () {

	filterTeensWithBMI("Files/source_file.txt", "Files/dest_file.txt");
	
	return 0;
}