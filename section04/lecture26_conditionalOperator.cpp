#include <iostream>

int main (int argc, char const *argv[]) {

  int a, b, c, result, max;

  std::cout << "Input first number: ";
  std::cin >> a;

  std::cout << "Input second number: ";
  std::cin >> b;

  std::cout << "Input third number: ";
  std::cin >> c;
/*
  if (a > b) {
    result = a + b;
  } else {
    result = a - b ;
  }
  std::cout << "result = " << result << std::endl;
*/

  // Conditional Operator symbles: ? and :
  // They are ternary operators. Thus, you must supply 3 operands.
  // Use: (condition)?(Expression for true):(Expression for false), so
  result = (a > b)?(a + b):(a - b);
  std::cout << "result = " << result << std::endl;

  // Use: to find the maximum number between a, b, and c
  max = (a > b && a < c)?a:((b > c)?b:c);
  std::cout << "max = " << max << std::endl;

  // Leap year example
  int year {};
  std::cout << "Enter the year: ";
  std::cin >> year;
  std::cout << (((year % 4 == 0 && year % 100 != 0) || (year % 400 == 0))?
               "Yes, it is a leap year":"No, it is not a leap year")
               << std::endl;

  return 0;
}
