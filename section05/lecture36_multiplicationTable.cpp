#include <iostream>
// program to print a multiplication table

int main (int argc, char const *argv[]) {

  int n {0};
  std::cout << "Enter a number: ";
  std::cin >> n;

  for (int i = 0; i < 10; ++i) {
    std::cout << n << " X " << i << " = " << n*i << std::endl;
  }

  return 0;
}
