#include <iostream>
#include <cmath>

// Please see the problem statement above to understand what you should do.
// ---------  WRITE YOUR FUNCTION BELOW -----------------

void series_sum(double x, unsigned n, double &sum) {
    
    for (int i = 0; i <= n; ++i) {
        sum += pow(x,i);
    }
    
}

int main () {

  double sumSeries {0}, first {2};
  int second {2};

  series_sum(first, second, sumSeries);

  std::cout << "The result of the series is: " << sumSeries << std::endl;

  return 0;
}
