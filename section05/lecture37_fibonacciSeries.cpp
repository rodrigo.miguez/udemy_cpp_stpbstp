#include <iostream>
// program to print the Fibonnaci seires

int main (int argc, char const *argv[]) {

  int n {0}, fibA {-1}, fibB {1}, buffer {0};
  std::cout << "Enter the number of terms: ";
  std::cin >> n;

  for (int i = 0; i < n; ++i) {
    buffer = fibA + fibB;
    fibA = fibB;
    fibB = buffer;
    std::cout << buffer << std::endl;
  }

  return 0;
}
