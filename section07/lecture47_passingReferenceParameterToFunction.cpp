#include <iostream>

void fun (int &, int &); // & is used the declare a reference variable

int main (int argc, char const *argv[]) {

  int x, y;
  x = 20; y = 30;
  std::cout << "Before calling the function: x = " << x << ", y = " << y << std::endl;
  fun(x,y);
  std::cout << "After calling the function:  x = " << x << ", y = " << y << std::endl;

  return 0;
}

void fun (int &a, int &b) { // now &a and &b are references to x and y in the main function
                            // they are actually pointing to the memory location of x and y
                            // and will change this specific memory location. Thus, x and y
                            // will be changed ----> passing parameter as reference
                            // INFO: a function can return only a single value not more than that
                            //       therefore passing a parameter as a reference to reflect back
                            //       changes to more than one variable.
  a += 5;
  b -= 10;
}
