#include <iostream>

int main (int argc, char const *argv[]) {

  int i {0};
  while (i < 10) {
    std::cout << "[" << i << "] " << "Hello World!" << std::endl;
    ++i;
  }

  return 0;
}
