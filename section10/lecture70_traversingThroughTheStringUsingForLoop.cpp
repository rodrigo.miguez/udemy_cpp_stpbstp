#include <iostream>
#include <string>

using namespace std;

int main () {

  string str = "Hello World";
  cout << str.at(0) << ", " << str.at(str.length()-1) << endl;
  //cout << str[0] << ", " << str[str.length()-1] << endl;

/*
  for (int i = 0; i < str.length(); i++) {
    cout << str.at(i) << ", ";
    // To transform into upper case:
    if (str[i] >= 'a' && str[i] <= 'z') {
      str[i] -= 32;
    }
  }
  cout << endl;

  cout << str << endl;
*/

  // Another way to do it:
  for (char &i:str) {
    cout << i << ", ";
    // To transform into upper case:
    if (i >= 'a' && i <= 'z') {
      i -= 32;
    }
  }
  cout << endl;

  cout << str << endl;

  return 0;
}
