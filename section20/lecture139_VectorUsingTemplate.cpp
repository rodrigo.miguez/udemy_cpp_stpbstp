#include <iostream>
#include <sstream>
#include <fstream>

using namespace std;

// IMPORTANT: When using dynamic memory allocation in your class you must take care of:
//            - Destructor
//            - Assignment operator
//            - Copy Constructor

template<typename T>
class Vector {
  private:
  // base address of the array
  T *item;
  // size of the array
  int size;
  // number of elements in the array
  int numElements;

  public:
  // overloaded constructor
  Vector(int size) {
    this->size = size;
    this->numElements = 0;
    this->item = new T[this->size];
  }
  // default constructor
  Vector() : Vector(10) { // : to redirect the call to the first constructor
  }
  // copy constructor: using deep copy
  Vector(const Vector<T>& rho) {
    this->item = new T[rho.size];
    this->size = rho.size;
    this->numElements = rho.numElements;
    for (int i = 0; i > this->size; i++) {
      this->item[i] = rho.item[i];
    }
  }
  // destructor: must be called to delete the dynamic allocated memory
  ~Vector() {
    delete [] item;
  }
  void push_Back(T v);
  int getSize() const;
  int getNumElements() const;

  // overload the subscription operator [] returning the reference of the element in '[]'
  T& operator[](int index);

  template<typename U>
  friend ostream& operator<<(ostream& _cout, const Vector<U>& v);

  Vector<T>& operator=(const Vector<T>& rho);
};

template<typename T>
void Vector<T>::push_Back(T v) {
  if (numElements >= size) {
    // double the size of the array keepinng the existing elements intact
    int newSize = size * 2;
    T *temp = new T[newSize];
    for (int i = 0; i < numElements; i++) {
      temp[i] = item[i];
    }
    delete [] item; // after item is deallocated temp becomes a dailing pointer (invalid location)
    item = temp;    // now it works
    this->size = newSize;
    //cout << "Array size doubled..." << this->size << endl;
  }
  item[numElements++] = v;
  //cout << "Added: " << v << endl;
  //cout << "Total elements in the vector: " << numElements << endl;
}

template<typename T>
int Vector<T>::getSize() const {
  return this->size;
}

template<typename T>
int Vector<T>::getNumElements() const {
  return this->numElements;
}

template<typename T>
T& Vector<T>::operator[](int index) {
  //if (index >= 0 && index < numElements) {
    return item[index];
  //} else {
    // Throw exception handling error: Not implemented now because we didn't learned yet
  //}
}

template<typename U>
ostream& operator<<(ostream& _cout, const Vector<U>& v) {
  _cout << "[";
  for (int i = 0; i < v.numElements; i++) {
    if (i == v.numElements - 1) {
      _cout << v.item[i];
    } else {
      _cout << v.item[i] << ",";
    }
  }
  _cout << "]";
  return _cout;
}

// Perfoms a deep copying
template<typename T>
Vector<T>& Vector<T>::operator=(const Vector<T>& rho) {
  if (this != &rho) {
    delete [] this->item;
    this->item = new T[rho.size];
    this->size = rho.size;
    this->numElements = rho.numElements;
    for (int i = 0; i < this->size; i++) {
      this->item[i] = rho.item[i];
    }
  }
}

int main () {

  Vector<int> v1(3);
  v1.push_Back(10);
  v1.push_Back(20);
  cout << v1 << endl;

  Vector<string> v2(3);
  v2.push_Back("aaa");
  v2.push_Back("bbb");
  cout << v2 << endl;

  return 0;
}
