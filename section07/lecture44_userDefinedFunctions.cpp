#include <iostream>

void msg () { // msg is the name of the function and void is the return type
  std::cout << "Hello World!" << std::endl;
  std::cout << "Thank you!" << std::endl;
}

void fun1() {
  std::cout << "Inside fun1." << std::endl;
  msg();
  std::cout << "Back in fun1." << std::endl;
}

int main (int argc, char const *argv[]) {

  fun1(); // Calling the function fun1
  std::cout << "Will call again." << std::endl;
  msg(); // Calling the function msg

  return 0;
}
