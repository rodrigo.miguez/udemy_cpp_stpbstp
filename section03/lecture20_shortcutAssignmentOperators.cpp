#include <iostream>

int main(int argc, char* argv[])
{

  int a {10};
  int b {20};

  std::cout << "a = " << a << ", b = " << b << std::endl;
  a += b; // a = a + b;
  std::cout << "(a += b;) a = " << a << std::endl;

  a -= b; // a = a - b;
  std::cout << "(a -= b;) a = " << a << std::endl;

  a *= b; // a = a * b;
  std::cout << "(a *= b;) a = " << a << std::endl;

  a /= b; // a = a / b;
  std::cout << "(a /= b;) a = " << a << std::endl;

  a %= b; // a = a % b;
  std::cout << "(a %= b;) a = " << a << std::endl;

  return 0;
}
