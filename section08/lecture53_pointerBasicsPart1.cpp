#include <iostream>

int main(int argx, char const *argv[]) {

  int temp = 300;
  std::cout << "Address of the variable temp: &temp = " << &temp << std::endl;
  std::cout << "Address of the variable temp: (unsigned long long)&temp = "
            << (unsigned long long)&temp << std::endl;

  return 0;
}
