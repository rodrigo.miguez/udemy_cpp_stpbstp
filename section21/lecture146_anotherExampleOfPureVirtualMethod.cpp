#include <iostream>

using namespace std;

class Bird {
  protected:
  string name;

  public:
  Bird(string name) {
    this->name = name;
  }
  string getName() {
    return this->name;
  }
  virtual void startFlight() = 0;
  virtual void stopFlight() = 0;

};

class Eagle:public Bird {
  private:
  string masterName;

  public:
  Eagle(string name, string masterName):Bird(name) {
    this->masterName = masterName;
  }
  void startFlight() {
    cout << "Eagle " << Bird::getName()
         << ", started flight in its own way" << endl;
  }
  void stopFlight() {
    cout << "Eagle " << Bird::getName()
         << ", stopped flight in its own way" << endl;
  }
};

class Owl:public Bird {
  private:
  int nightVisionPower;

  public:
  Owl(string name, int nightVisionPower):Bird(name) {
    this->nightVisionPower = nightVisionPower;
  }
  void startFlight() {
    cout << "Owl " << Bird::getName()
         << ", started flight at low height" << endl;
  }
  void stopFlight() {
    cout << "Owl " << Bird::getName()
         << ", stopped flight on a tree" << endl;
  }

};

class Game {
  // Here we see again the dynamic polymorphism in action:
  // The pointer is created w.r.t. the base class Bird and we assigne the subtype objects in each
  // element of the birds[] array. Then we call the start or stop flight methods using the super
  // type pointers so the compiler looks into the pure virtual method during the compiled time and
  // during the execution time they are overwritten by the method in the subtype objects. which hets
  // the call.
  private:
  Bird* birds[3];

  public:
  Game() {
    birds[0] = new Eagle("Super Falcon", "John");
    birds[1] = new Owl("Night Captain", 10);
    birds[2] = new Eagle("Knight", "Alice");
  }
  void playGame() {
    birds[0]->startFlight();
    birds[1]->startFlight();
    birds[2]->startFlight();

    birds[0]->stopFlight();
    birds[1]->stopFlight();
    birds[2]->stopFlight();
  }
};

int main () {

  Game game;
  game.playGame();

  return 0;
}

