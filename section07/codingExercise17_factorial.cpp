#include <iostream>
#include <cmath>

unsigned factorial(unsigned n){
    unsigned fact = 1;
    for(unsigned i = n; i >= 1; fact *= i--);   // notice the use of post decrement -- operator in the modifier section
                                            // I have terminated that for loop with that semi colon
    return fact;
}
// -- Please read the problem statement --
// The name of the function must be series_sum and it must receive the parameters as described in the problem statement
// ------------ WRITE YOUR CODE AFTER THIS LINE ---------------------
void series_sum (double x, unsigned n, double &sum) {
    int factorialNumber {0};
    for (int i = 0; i <= n; ++i) {
        factorialNumber = factorial(i);
        sum += pow(x,i)/factorialNumber;
    }
}

int main () {

  double sumSeries {0}, first {2};
  int second {2};

  series_sum(first, second, sumSeries);

  std::cout << "The result of the series is: " << sumSeries << std::endl;

  return 0;
}
