#ifndef CODINGEXERCISE45_H
#define CODINGEXERCISE45_H
#include <vector>
// a Distance can be represented using feet and inches, the following struct type 
// Distance can be used to represent such a distance in memory
struct Distance{
    int feet;   // for holding feet value for the distance
    double inches;  // for holding inches value for the distance
};

#endif
