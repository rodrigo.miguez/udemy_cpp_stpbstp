#ifndef EMPLOYEE_H
#define EMPLOYEE_H

#include <iostream>
#include <vector>
#include <string>

using namespace std;

class Employee {

  private:
  string name;
  vector <int> hoursWorked;
  double rate;
  mutable bool is_added;
  mutable double t_salary;

  public:
  Employee(string name, double rate);
  string getName() const;
  double getRate() const;
  void setRate(double rate);
  void addHours(int hours);
  double getSalary() const;

};

void printEmployee(const Employee& e);

#endif // EMPLOYEE_H
