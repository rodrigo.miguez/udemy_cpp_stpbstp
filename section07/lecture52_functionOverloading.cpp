#include <iostream>
#include <cmath>

int sum(int, int);
double sum(double, double);
int sum(int, int, int);

// In fuction overloading (or polymorphism) we can have two or more functions with the same name
// but the signatures must be different (int/doubles), or the number of parameters must be
// different.
// ADDITIONALLY: Which particular form of the function that will be attached to the call is
// resolved during compilation, Thus this is called compiled time polymorphism.
int sum(int a, int b) {
  std::cout << "Integer sum" << std::endl;
  return a + b;
}

double sum(double a, double b) {
  std::cout << "Double sum" << std::endl;
  return a + b;
}

int sum(int a, int b, int c) {
  std::cout << "3 integer sum" << std::endl;
  return a + b + c;
}

int main (int argc, char const *argv[]) {

  std::cout << sum(3, 7) << std::endl;
  std::cout << sum(3.4, 7.5) << std::endl;
  std::cout << sum(3, 4, 5) << std::endl;
  std::cout << sum(3, 4) << std::endl;

  return 0;
}
