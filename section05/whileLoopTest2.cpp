#include <iostream>

using namespace std;

int main () {
    // --- DO NOT CHANGE ABOVE THIS LINE
    // n will be passed here by the auto tester, YOU DO NOT NEED TO DECLARE OR INITIALIZE n

    // Instruction 1: Write a while loop to find the sum of all integer numbers from 1 to n which are divisible by 5
    // and not divisible by 2.
    // Please note that,
    int n;

    cout << "Enter a value: ";
    cin >> n;

    int sum = 0;    // You need to use this sum variable to keep the sum
    int i = 0;
    while (i <= n) {
        if (i % 5 == 0 && i % 2 != 0) {
            sum += i;
        }
        ++i;
    }
    cout << "The sum of all integer numbers from 1 to " << n << " which \n"
         << "are divisible by 5 and not divisible by 2 is: " << sum << endl;

    return 0;  // DO NOT CHANGE THIS AS THE AUTO-TESTER WILL CHECK THE VALUE OF THIS SUM
}
