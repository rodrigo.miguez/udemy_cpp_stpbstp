#include <iostream>

using namespace std;

class Employee {
  protected:
  string name;
  int age;
  double basic;

  public:
  Employee(string name, int age, double basic) {
    this->name = name;
    this->age = age;
    this->basic = basic;
  }
  virtual double calculateSalary() {
    cout << "Calculating the salary for the employer." << endl;
    double salary = basic + basic * 0.1;
    return salary;
  }
};

class Manager: public Employee {
  private:
  double hra;

  public:
  Manager(string name, int age, double basic):Employee(name, age, basic) {
    hra = basic * 0.15;
  }
  double calculateSalary() {
    cout << "Calculating the salary for manager..." << endl;
    double salary = Employee::calculateSalary();
    salary += hra;
    return salary;
  }
};

class FactoryWorker: public Employee {
  private:
  double tiffinAllowences;

  public:
  FactoryWorker(string name, int age, double basic):Employee(name, age, basic) {
    tiffinAllowences = 0.05 * basic;
  }
  double calculateSalary() {
    double salary = Employee::calculateSalary();
    salary += tiffinAllowences;
    return salary;
  }
};

class Clerk: public Employee {
  private:
  int otHours;
  double otRate;

  public:
  Clerk(string name, int age, double basic, int otHours, double otRate):Employee(name, age, basic) {
    this->otHours = otHours;
    this->otRate = otRate;
  }
  double calculateSalary() {
    double salary = Employee::calculateSalary();
    salary += otRate * otHours;
    return salary;
  }

};


int main () {

  Manager *p = new Manager("Alice",25, 8000.0); // Employee *p = new Manager(...); is possible
  double salary = p->calculateSalary(); // .calculateSalary() doesnt work in this context
  cout << salary << endl;

  /*
  Manager p("Alice",25, 8000.0);
  double salary = p.calculateSalary();
  cout << salary << endl;
  */

  return 0;
}

