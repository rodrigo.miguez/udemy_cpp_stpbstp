#include <iostream>
#include <string>

using namespace std;

struct Account {
  int accNo;
  string holderName;
  double balance;
};

Account readAccountData() {
  Account acc;
  cout << "Input account number: ";
  cin >> acc.accNo;
  cout << "Input holders name: ";
  cin >> acc.holderName;
  cout << "Input inicial balance: ";
  cin >> acc.balance;
  return acc;
}

void printAccountData(Account acc) {
  cout << "Account details" << endl;
  cout << "--------------------------" << endl;
  cout << "Account number: " << acc.accNo << endl;
  cout << "Holder name: " << acc.holderName << endl;
  cout << "Balance: $" << acc.balance << endl;
}

bool accountDebit (Account& acc, double amount) {
  bool success = false;

  if (acc.balance >= amount) {
    acc.balance -= amount;
    success = true;
  }
  return success;
}

void accountCredit(Account& acc, double amount) {
  acc.balance += amount;
}

int main () {

  Account a;
  a = readAccountData();
  printAccountData(a);

  cout << "Input debit amount: ";
  double amount;
  cin >> amount;

  bool success = accountDebit(a, amount);
  if (success) {
    cout << "Account debited with $" << amount << " successfully..." << endl;
    cout << "Current balance: $" << a.balance << endl << endl;
  } else {
    cout << "Debit Unsuccessful, make sure that you have sufficient balance..." << endl;
  }


  return 0;
}
