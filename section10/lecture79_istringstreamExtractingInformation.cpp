#include <iostream>
#include <sstream>
#include <string>

using namespace std;

int main() {

/*
  string source = "10 20 30 40 50";

  istringstream iss (source); // iss is an object for the class istringstream

  int k;
  while(iss >> k) {
    cout << k << endl; 
  }
*/

/*
  // In case your string is separated by commma:
  string source = "10,20,30,40,50";

  istringstream iss (source); // iss is an object for the class istringstream

  int k;
  while(iss >> k) {
    cout << k << endl;
    if (iss.peek() == ',') {
      iss.ignore();
    }
  }
*/

/*
  // To extract as string:
  string source = "John,10,Tina,20,Robin,19";

  istringstream iss (source); // iss is an object for the class istringstream

  string part;
  while(getline(iss, part, ',')) {
    cout << part << endl;
  }
*/

  // To parse with different delimeters:
  string source = "John:10,Tina:20,Robin:19";

  istringstream iss (source); // iss is an object for the class istringstream

  string part;
  while(getline(iss, part, ',')) {
    //cout << part << endl;
    istringstream iss2 (part);
    string part2;
    while(getline(iss2, part2, ':')) {
      cout << part2 << endl;
    }
  }


  return 0;
}
