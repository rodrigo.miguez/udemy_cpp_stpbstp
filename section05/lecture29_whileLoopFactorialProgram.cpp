#include <iostream>
// write a program to find the factorial of a program
// 5! = 5 * 4 * 3 * 2 * 1
// 4! = 4 * 3 * 2 * 1
// 3! = 3 * 2 * 1
// 6! = 6 * 5!
// 0! = 1

int main (int argc, char const *argv[]) {

  int n;
  long fact {1};

  std::cout <<  "Enter a number: ";
  std::cin >> n;

  if ( n < 0) {
    std::cout << "ERROR: Ilegal value!" << std::endl;
    return EXIT_FAILURE;
  }

  int i {0};
  while (i < n) {
    fact *= (n-i);
    ++i;
  }
  std::cout  << n << "! = " << fact << std::endl;

  return 0;
}
