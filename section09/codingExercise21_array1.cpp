#include <iostream>

using namespace std;


void arrTest(int arr[], int n, int value1, int value2){
    // This function receives 4 parameters, the first one is an array of integers, I have declared
    // that array in the test caller, you do not need to declare that again. n is the number of elements in the array arr
    // so, if n is 5, then there are 5 elements  in arr, if n is 10 there are 10 elements in arr
    // Please do not change above this line
    for (int i = 0; i < n; ++i) {
        arr[i] = 0;
    }
    // Instruction 1: Assign value1 to the first element of the array arr, write this code just below this line.
    arr[0] = value1;

    // Instruction 2: Assign value2 to the last element of the array, write this code just after this line.
    arr[n-1] = value2;

    // Instruction 3: Make sure that all the other elements of the array are 0, for this you may need to do something
    //                prior doing the code for Instruction 1.

}
