#include <iostream>

int main (int argc, char const *argv[]) {

  for (int i = 0; i < 10; ++i) {
    std::cout << i+1 << ". Hello World!" << std::endl;
    // if a variable is declared inside of the loop, then it is not visible
    // outside of it. For example:
    // int j {1};
  }
  // std::cout << "The value of j is: " << j << std::endl;
  // This way j will be out of scope

  int i, j;
  for (i = 0, j = 9; i < 10; ++i, --j) {
    std::cout << i << " " << j << std::endl;
  } 

  return 0;
}
