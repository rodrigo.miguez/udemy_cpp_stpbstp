#include <iostream>
#include <iomanip>

using namespace std;

void display(int *p, int n) {
  for (int i = 0; i < n; i++) {
    cout << setw(4) << p[i];
  }
  cout << endl;

}

int main() {

  int x[100];

  cout << "Input how many integer element that you want to process: ";
  int n;
  cin >> n;

  int *p = new int[n];
  int *ptr = new int(10);

  for (int i = 0; i < n; i++) {
    cout << "Next int: ";
    cin >> p[i];
  }

  display(p, n);

  delete [] p;
  delete ptr;

  return 0;
}
