#include <iostream>

int main (int argc, char const *argv[]) {

  int x[] = {10, 20, 30, 40, 50};
  int *ptr = &x[0];
  for (int i = 0; i < sizeof(x)/sizeof(int); ++i) {
    std::cout << *(ptr + i) << " ";
  }
  std::cout << "\n" << std::endl;

  // A simpler way to do it:
  std::cout << "A simpler way to to it: " << std::endl;
  for (int i = 0; i < sizeof(x)/sizeof(int); ++i) {
    std::cout << *(x + i) << " ";
  }
  std::cout << "\n" << std::endl;

  // Another way to do it:
  std::cout << "Another way to to it: " << std::endl;
  for (int i = 0; i < sizeof(x)/sizeof(int); ++i) {
    std::cout << i[x] << " "; // Same as: a[i]
  }
  std::cout << std::endl;

  // The above way works because:
  // a[i] = *(a + i)
  // and
  // i[a] = *(i + a)
  // which turns out to be the same thing

  return 0;
}
