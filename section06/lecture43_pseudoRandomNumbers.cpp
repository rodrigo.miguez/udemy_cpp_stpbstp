#include <iostream>
#include <cstdlib>
#include "time.h"

int main (int argc, char const *argv[]) {

  // srand() function uses a seed to create the random number
  // srand(100): the seed is 100
  srand(time(NULL));
  // now rand() can be called
  for (int i = 0; i < 10; ++i) {
    //std::cout << rand() << std::endl;
    // if you want random numbers from 0 to 99
    std::cout << rand() % 100 << std::endl;
  }


  return 0;
}
