#include <iostream>
#include <cmath>
// program to find the prime numbers

int main (int argc, char const *argv[]) {

  int n {0};
  bool flag = true;
  std::cout << "Enter the number of terms: ";
  std::cin >> n;

  for (int i = 2; i <= sqrt(n); ++i) {
    if (n % i == 0) {
      flag = false;
      break;
    }
  }

  if (flag) {
    std::cout << "The given number is a prime number" << std::endl;
  } else {
    std::cout << "The given number is not a prime number" << std::endl;
  }

  return 0;
}
