#include <iostream>

int main(int argc, char const *argv[]) {

  // This piece of code was written assuming a c++14 standard

/*
  // Opitions for using "Natural" numbers
  int;              // An integer occupies 4 bytes of space in memory
  signed int;       // Signed means it incorporate both positive and negative values
  unsigned int;     // unsigned means that only positive values are allowed
  return 0;

  // Options for using Real numbers:
  float;            // 4 bytes of space in memory
  double;           // 8 bytes of space in memory
  long double;      // 10 bytes of space in memory

*/

  // Ways to initialize a variable
  double rate_of_interest0 = 0.07;  // Equals to opperator, inherited from C language
  double rate_of_Interest1 {0.05}; // Called "precise initialization", recommended in c++14

  std::cout << "Rate of interest 0 = " << rate_of_interest0 << std::endl
            << "Rate of interest 1 = " << rate_of_Interest1 << std::endl;

  // In c++11 and c++14 you can use the following when you are not sure about the datatytpe
  auto myVar {9.5}; // When using auto the variable must be initialized
  auto myIntVar {5};

  // Now verify the variable datatype (needs the header file <typeinfo>)
  std::cout << "Type of myVar: " << typeid(myVar).name() << std::endl
            << "Type of myIntVar: " << typeid(myIntVar).name() << std::endl;
            // .name() is the method that is going to return the type of the variable assigned

  return 0;
}
