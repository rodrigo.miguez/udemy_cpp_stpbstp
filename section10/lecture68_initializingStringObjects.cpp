#include <iostream>
#include <string>

using namespace std;

int main () {

  string str1 ("Robin Williams");
  cout << str1 << endl;
  cout << str1.length() << endl;

  string str2 (str1);
  cout << "str2 = " << str2 << endl;

  str1 = "Welcome to C++ at Udemy!";
  cout << str1 << endl;
  cout << str1.length() << endl;

  char ch = '@';
  unsigned n = 15;

  string str3 (n, ch);
  cout << "str3 = " << str3 << endl;

  return 0;
}
