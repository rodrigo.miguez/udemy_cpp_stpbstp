#include <iostream>

int main(int argx, char const *argv[]) {

  int temp1 {300}, temp2 {353};
  std::cout << "Working with integers!" << std::endl;
  std::cout << "Address of temp1: " << (unsigned long long)&temp1 << std::endl;
  std::cout << "Address of temp2: " << (unsigned long long)&temp2 << std::endl;

  int *t1 = &temp1;
  std::cout << "Content of t1: " << (unsigned long long)t1 << std::endl;
  std::cout << "Contenct of the location: " << *t1 << std::endl;

  t1 = t1 + 1; // jump one integer locations
  std::cout << "Now perform pointer arithmetic: t1 = t1 + 1;" << std::endl;
  std::cout << "Content of t1: " << (unsigned long long)t1 << std::endl;
  std::cout << "Contenct of the location: " << *t1 << std::endl;

  // Now we do the same using doubles
  std::cout << std::endl;

  std::cout << "Working with doubles!" << std::endl;
  double temp3 {10.5}, temp4 {15.2};
  std::cout << "Address of temp3: " << (unsigned long long)&temp3 << std::endl;
  std::cout << "Address of temp4: " << (unsigned long long)&temp4 << std::endl;

  double *t3 = &temp3;
  std::cout << "Content of t3: " << (unsigned long long)t3 << std::endl;
  std::cout << "Contenct of the location: " << *t3 << std::endl;

  t3 = t3 + 3; // jump three double locations
  std::cout << "Now perform pointer arithmetic: t3 = t3 + 3;" << std::endl;
  std::cout << "Content of t3: " << (unsigned long long)t3 << std::endl;
  std::cout << "Contenct of the location: " << *t3 << std::endl;

  // Lecture 57:
  // char *k = t1; (This gives us an error because we are trying to assign a charecter pointer to a
  //                integer pointer).
  // To avoid this problems we can use explicit casting:
  char *k = (char *)t1;
  // Now, since the pointer k is a character pointer (1 byte), it will make reference to the first
  // byte to &temp2. Which in binary is: 0110 0001 <- this as a character type represents 'a'
  std::cout << std::endl;
  std::cout << "*k is: " << *k << std::endl;

  return 0;
}
