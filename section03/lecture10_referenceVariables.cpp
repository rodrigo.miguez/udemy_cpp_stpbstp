#include <iostream>

int main(int argc, char* argv[])
{
  int x {10};

  // In this lecture we learn how to declare a reference variable
  int &v = x; // The "&" previous to the variable name is used to declare a reference variable
              // All the reference variable must be initialized at their declaration
              // Now v can be used to manipulate the content of x
              // Both v and x must have the same type
              // v is pointing to x in memory
              // v is an alias of x

  std::cout << "Before: x = " << x << std::endl;

  v = v + 1;

  std::cout << "After: x = " << x << std::endl;  

  return 0;
}
