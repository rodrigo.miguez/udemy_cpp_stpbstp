#include <iostream>
// write a program to find the sum of numbers

int main (int argc, char const *argv[]) {

  int n, sum {0}, counter {0};
  double avg;

  std::cout <<  "Enter any number (0 to terminate): ";
  std::cin >> n;

  while (n != 0) {
    sum += n;
    ++counter;
    std::cout <<  "Enter any number (0 to terminate): ";
    std::cin >> n;
  }
  avg = (double)sum / counter;
  std::cout  << "The sum of the numbers is:  " << sum << "\n"
             << "The avegare of the numbers is: " << avg << std::endl;

  return 0;
}
