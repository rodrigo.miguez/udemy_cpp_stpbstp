#include <iostream>

int main(int argc, char* argv[])
{

  int first {10};
  int second {20};

  double result;

  result = first / second;

  std::cout << "Before: Result = " << result << std::endl;
  // Since both first and second are doubles, the printed result is 0
  // Operations between integers will always give an integer
  // If second was a double, then we result would be given in a doubles
  // The result is always in the datatype of the higher precedence type

  result = (double)first / second; // Here we are using the explicit casting
                                   // The compiler knows that at this operation
                                   // it should convert first fron int to double

  std::cout << "After: Result = " << result << std::endl;

  return 0;
}
