#include <iostream>
#include <iomanip>

using namespace std;

void fun(int n) {

  int *p = new int[n];

  time_t t = time(NULL);
  srand(t);

  for (int i = 0; i < n; i++) {
    p[i] = rand() % 100;
  }
  // doing some task here with the block pointed by p

  // Must delete *p using:
  delete [] p;

  // Or if one still needs the memory, then the pointer should be
  // returned to the caller by changing the function to: int * fun(int n)
  // and adding: return p;

}

int main() {

  fun(10000);
  fun(5000);

  // int *ptr = fun(10000);
  //...
  // delete [] ptr;

  return 0;
}
