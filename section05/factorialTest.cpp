#include <iostream>
using namespace std;


int main () {
    // --- YOU NEED TO FIND THE FACTORIAL OF THAT n supplied to this function as parameter,
    // --- The auto tester will supply various n and will test the function.
    int n;

    cout << "Enter a number: ";
    cin >> n;
    // Instruction 1: You need to check for valid n first, if n is invalid that is if n is negative number
    //                your program should print "INVALID INPUT." with a new line at the end and terminate (Nothing else).
    if (n < 0) {
        cout << "INVALID INPUT." << endl;
        return 0;
    }

    // Instruction 2: If n is valid, that is >=0, then your program should print the factorial of n after calculation

    //                Example output1: If the value of n is 5 (say) then your program should print exactly:
    //                Factorial of 5 is 120
    //                There must be a newline at the end.

    //                Example output 2: If the value of n is -1, then your program should print exactly:
    //                INVALID INPUT
    //                With a new line at the end

    //                Example output 3: If the value of n is 3, then your program should print exactly:
    //                Factorial of 3 is 6
    //                With a new line at the end

    // --- PLEASE DO NOT CHANGE ABOVE THIS LINE ---

    // Write your code below.
    int fact {1};

    int i {0};
    while (i < n) {
        fact *= (n-i);
        ++i;
    }
    cout << "The factorial of " << n << " is " << fact << "." << endl;

      return 0;
}
