#include <iostream>
#include <cmath>

bool isPrimeNumber(int n) {

  int count {2};
  for (int i = 2; i <= sqrt(n); ++i) {
    if (n % i == 0) {
      return false;
      }
  }

  return true;
}

int main (int argc, char const *argv[]) {

  /*
  int n;
  std::cout << "Enter any number: ";
  std::cin >> n;

  if (isPrimeNumber(n)) {
    std::cout << "The number is a prime number!" << std::endl;
  } else {
    std::cout << "The number is not a prime number!" << std::endl;
  }
  */

  int n;
  for (n = 10; n <= 1000; ++n) {
    if (isPrimeNumber(n)) {
      std::cout << n << std::endl;
    }
  }

  return 0;
}
