#include <iostream>
// When compiling the compiler will start the compilation from the first line of the code
// if the function is declared before the main function, then it will compile without any errors
// when it reaches the function call totalPay(...) inside of the main function because the compiler
// already ran into the totalPay function before. However, if you declare the totalPay function
// after the main function, the compiler will throw an error at the totalPauy function call
// inside of the main function. To avoid that we use a PROTOTYPE DECLARATION at the beginning
// of the code:
double totalPay(int, double);

int main (int argc, char const *argv[]) {

  int hours_worked;
  double rate;

  hours_worked = 41;
  rate = 10.0;

  double total = totalPay(hours_worked, rate);
  std::cout << "Total pay = " << total << std::endl;

  hours_worked = 47;
  rate = 10.5;
  total = totalPay(hours_worked, rate);
  std::cout << "Total pay = " << total << std::endl;

  return 0;
}

double totalPay(int hoursWorked, double ratePerHour) {
  double total = hoursWorked * ratePerHour;
  if (hoursWorked > 40) {
    total = total + (hoursWorked - 40) * 2;
  }

  return total;
}
