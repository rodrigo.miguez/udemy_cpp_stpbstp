#include <iostream>
#include <string>

using namespace std;

bool isValidCreditCard(string cc_number) {
  bool m_isValid = false;

  if (cc_number.length() != 16) {return m_isValid;}

  int cc_array[16];
  for (int i = 0; i < 16; i++) {
    cc_array[i] = cc_number.at(i)-48;
    //cout << cc_array[i];
  }
  //cout << endl;

  // Double every 2nd number beginning from right
  // and sum numbers with more than 1 digit
  for (int i = 14; i >= 0; i--) {
    cc_array[i] = 2 * cc_array[i];
    if (cc_array[i] > 9 ) {
      cc_array[i] = 1 + cc_array[i] % 10;
    }
    i--;
  }

  int sum = 0;
  for (int i = 0; i < 16; i++) {
    sum += cc_array[i];
  }

  if (sum % 10 == 0) {m_isValid = true;}

  return m_isValid;
}

int main () {

  string str = "4939309281821523";
  cout << isValidCreditCard(str) << endl;

  return 0;
}
