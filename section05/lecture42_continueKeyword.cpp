#include <iostream>
#include <cmath>
// program to find the prime numbers

int main (int argc, char const *argv[]) {

  for (int i = 0; i < 100; ++i) {
    if (i % 3 != 0 || i == 0) {
      continue;
    }
    std::cout << i << std::endl;
  }

  return 0;
}
