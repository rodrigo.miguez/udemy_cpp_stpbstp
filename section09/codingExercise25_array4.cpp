#include <iostream>

/**
 * Array arr[] will contain n number of integers in the range 0 - 99
 * You need to count the numbers in the specified ranges and put the result in the cpunter array
 * See the Problem statement above for details.
 *
 */
void countValuesInRange(int arr[], int n, int counter[]){
    // Do not forget to initialize each element of counter array,
    // You will need to iterate the arr array and count the elements and update specific element of the counter array
    // auto-tester will call this function with various values in arr array, auto tester will create counter array as well,
    // please note that n is the number of elements in arr array, the counter array will always have 10 elements.
    // ----WRITE YOUR CODE AFTER THIS LINE, DO NOT CHANGE ANYTHING ABOVE ----
    for (int i = 0; i < 10; ++i) {counter[i] = 0;}
    /*
    // NAIVE WAY:
    for (int i = 0; i < 10; ++i) {
        for (int j = 0; j < n; ++j) {
            if ( arr[j] >= i*10 && arr[j] <= i*10+9) {
                ++counter[i];
            }
        }
    }
    */
    // TODO: Implement without any if-else statements
    for (int i = 0; i < n; ++i) {++counter[arr[i]/10];}

}

int main() {

  int x[15] = {7, 77, 45, 5, 91, 90, 40, 44, 12, 45, 57, 58, 52, 21, 51};
  int c[10];

  countValuesInRange(x, 15, c);

  std::cout << "cout[] = {";
  for (int i = 0; i < 10; ++i) {
    std::cout << c[i] << " ";
  }
  std::cout << "}\n";

  return 0;
}
