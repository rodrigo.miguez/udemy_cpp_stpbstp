#include <iostream>

int main (int argc, char const *argv[]) {

  int n;
  std::cout << "Enter integer: ";
  std::cin >> n;

/*
  if (n == 1) {
    std::cout << "One." << std::endl;
  } else if (n == 2 ) {
      std::cout << "Two." << std::endl;
  } else if (n == 3) {
    std::cout << "Three." << std::endl;
  } else {
    std::cout << "Invalid number for us." << std::endl;
  }
*/

  switch (n) {
    case 1:
      std::cout << "One." << std::endl;
      break;
    case 2:
      std::cout << "Two." << std::endl;
      break;
    case 3:
      std::cout << "Three." << std::endl;
      break;
    default:
      std::cout << "Invalid number for us." << std::endl;
      break; // default can be at the beginning but must use break;
             // at the end of the switch case break is not needed.
  }

  return 0;
}
