#include <iostream>

// *arr passes the address of the first element of the array (base address) as pointer.
//  arr[] is another way to pass the address (sending the name of the array).
void printArray(int *arr, int n) {

  for (int i = 0; i < n; ++i) {
    std::cout << *(arr + i) << " ";
  }
  std::cout << "\n";

}

int main(int argc, char const *argv[]) {

  int a[5] = {10, 20, 30, 40, 50};
  printArray(a, 5);

  return 0;
}
