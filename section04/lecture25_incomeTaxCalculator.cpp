#include <iostream>
#include <cstdlib>
#include <iomanip>

int main (int argc, char const *argv[]) {

  double income {}, taxRate {};
  std::cout << "Enter the income: ";
  std::cin >> income;

  if ( income < 0 ) {
    std::cout << "This amount of income is invalid" << std::endl;
    return EXIT_FAILURE;
  } else if (income >= 0 && income < 5000) {
    taxRate = 0.00;
  } else if (income >= 5000 && income < 10000) {
    taxRate = 0.06;
  } else if (income >= 10000 && income < 20000) {
    taxRate = 0.12;
  } else if (income >= 20000 && income < 50000) {
    taxRate = 0.20;
  } else {
    taxRate = 0.30;
  }
    std::cout << "The total tax is: $" << std::setprecision(2) << std::fixed
                                       << income * taxRate << std::endl;

  return 0;
}
