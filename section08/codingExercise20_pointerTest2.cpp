// PLEASE FOLLOW THE INSTRUCTION NUMBERS AND DO THEM CHRONOLOGICALLY
#include <iostream>

using namespace std;

// Instruction 3: Write a function fun1 according to the call you made at instruction 1 of line 18.
// In this function you must use the parameter received to increment the value at the original
// location by 10.
void fun1(int *ptr) {
  *ptr = *ptr + 10;
}

// Instruction 4: Write a function fun2 according to the call you made at instruction 2 of line 24.
// In this function you must use the parameter received to increment the value at the original
// location by 1.0.
void fun2(double *ptr) {
  *ptr = *ptr + 1.0;
}

void funMain(int value1, double value2){
// DO NOT CHANGE ABOVE THIS LINE
// Instruction 1: Call function fun1 and pass the address of value1 to fun1. Write code just after
// this line.
  fun1(&value1);

  cout << "Value1: " << value1 << endl;   // NO CHANGE IN THIS LINE

// Instruction 2: Call function fun2 and pass the address of value2 to fun2. Write code just after
// this line.
  fun2(&value2);

  cout << "Value2: " << value2 << endl;   // No change in this line
}
