#include <iostream>

using namespace std;

struct Student {
  int student_id;
  string name;
  double grade_point;
};

int main () {

  Student s1, s2;
  s1.student_id = 1;
  s1.name = "John Doe";
  s1.grade_point = 9.95;

  s2.student_id = 2;
  s2.name = "Terry Myers";
  s2.grade_point = 8.95;

  cout << "Id: " << s1.student_id << ", "
       << "Name: " << s1.name << ", "
       << "Grade Point: " << s1.grade_point << endl;

  return 0;
}
