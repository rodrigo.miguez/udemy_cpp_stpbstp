#include <iostream>
#include <string>

using namespace std;

int main () {

  // if string is a single word:
  /*
  string name;
  cout << "Input your name: ";
  cin >> name;
  cout << "Welcome, " << name << "!" << endl;
  */

  // else:
  string name;
  cout << "Input your name: ";
  getline(cin, name);
  cout << "Welcome, " << name << "!" << endl;

  return 0;
}
