#include <iostream>
using namespace std;

void printArray(int arr[], int n){

    // please do not change above this line

    // Instruction 1: You will write a loop to print the content of the array arr passed into this function.
    //                There should be only one space after each integer you print and there must be only one newline
    //                at the end of printing all the numbers. n being the number of elements in arr
    //                So, just for example: if arr contains [10, 20, 30, 40, 50] then n = 5 and your loop should print
    //                10 20 30 40 50
    //                There should be a newline at the end of the above line when printed.

    //  Write your code below reading the instruction above
    for (int i = 0; i < n; ++i) {
        cout << arr[i] << " ";
    }
    cout << "\n";

}
