#include <iostream>
#include <string>

using namespace std;

struct Employee {
  int empId;
  string name;
  double salary;
};

void inputEmployee(Employee e[], int size) {
  for (int i = 0 ; i < size; i ++) {
    cout << "Emp Id: ";
    cin >> e[i].empId;
    cout << "Emp Name: ";
    cin >> e[i].name;
    cout << "Salary: ";
    cin >> e[i].salary;
  }
}

void printEmployee(Employee *e, int size) { // using Employee *e or Employee e[] has the same effect
  cout << "Employee Details" << endl;
  cout << "-----------------------------" << endl;
  for (int i = 0; i < size; i++) {
    cout << "Employee Id: " << e[i].empId << endl
          << "Name: " << e[i].name << endl
          << "Saraly: " << e[i].salary << endl;
    cout << "-----------------------------" << endl;
  }
}

int main () {

  int size = 2;
  Employee e[size];
  /*
  e[0].empId = 1;
  e[0].name = "John";
  e[0].salary = 5000.0;

  e[1].empId = 2;
  */

  inputEmployee(e, size);
  cout << endl;

  printEmployee(e, size);
  cout << endl;

  // Another way to access the elements members:
  cout << "Access EmpId of e[0] using arrow operator: " << e->empId << endl; // access EmpId of e[0]
  cout << "Access EmpId of e[1] using arrow operator: " << (e+1)->empId << endl; // access EmpId of e[1], and so on


  return 0;
}
