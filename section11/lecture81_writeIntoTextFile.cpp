#include <iostream>
#include <fstream>
#include <cstdlib>

using namespace std;

int main() {

  ofstream fout; // fout is the object of ofstream class that belongs to fstream header file
  fout.open("Files/l81file.txt");

if (fout.fail()) {
    cout << "Error in opening file" << endl;
    exit(1);
  }

  fout << "Hello World!" << endl;
  fout << "This will be the second line in the file" << endl;
  int a = 35;
  double p = 35.75;
  fout << "a = " << a << ", p = " << p << endl;

  fout.close();

  return 0;
}
