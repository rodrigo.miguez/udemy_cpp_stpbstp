#include <iostream>
#include <string>

using namespace std;

struct Employee {
  int empId;
  string name;
  double salary;
};

int main () {

  /*
  Employee e;
  Employee* eptr = &e;

  eptr->empId = 5;
  eptr->name = "John";
  eptr->salary = 5000.00;

  cout << eptr->empId << endl;
  cout << e.name << endl;
  cout << e.salary << endl;
  */

  // Using dynamic allocation
  Employee *p = new Employee;
  p->empId = 5;
  p->name = "John";
  p->salary = 5000.00;

  cout << p->empId << endl;
  cout << p->name << endl;
  cout << p->salary << endl;

  (*p).empId = 10;
  (*p).name = "Terry";
  (*p).salary = 10000.00;

  cout << p->empId << endl;
  cout << p->name << endl;
  cout << p->salary << endl;

  delete p;

  return 0;
}
