#include <iostream>

const int array_size = 5;

// The pass the reference of an array:
// If the array that is being passed in the fuction has size different from array_size, then the
// compiler will report an error.
void printArray(int (&arr)[array_size]) {


  for (int i = 0; i < array_size; ++i) {
    std::cout << *(arr + i) << " ";
  }
  std::cout << "\n";

}

int main(int argc, char const *argv[]) {

  int a[5] = {10, 20, 30, 40, 50};
  printArray(a);

  return 0;
}
